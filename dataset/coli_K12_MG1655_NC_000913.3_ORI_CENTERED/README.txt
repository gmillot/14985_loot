Ecoli-K12-MG1655_ORI_CENTERED.fasta same as 
Z:\reference_genomes\coli_K12_MG1655_NC_000913.3\Ecoli-K12-MG1655.fasta
But genome centered on the OriC

tss_ORI_CENTERED.txt same as 
Z:\reference_genomes\coli_K12_MG1655_NC_000913.3\tss.txt
But genome centered on the OriC
1) le nom du gène associé au promoteur, 2) l’ID du promoteur, 3) son orientation et 4) sa position.

cds_ORI_CENTERED.txt same as 
Z:\reference_genomes\coli_K12_MG1655_NC_000913.3\NC_000913_CDS.csv
But genome centered on the OriC
column: "gene", "start", "end", "length", "interval", "cds.orient"
I do not know what is interval

Features in:
https://www.ncbi.nlm.nih.gov/nuccore/U00096

OriC:
3925744  3925975
