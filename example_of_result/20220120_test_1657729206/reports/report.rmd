---
title: 'Insertion Sites Report'
author: 'Gael Millot'
date: ''
output:
  html_document:
    toc: TRUE
    toc_float: TRUE
---

\n\n<br /><br />\n\n

<br /><br />

### Removal for reads made of N only




Number of sequences before removing reads with only N: 10,000

Number of sequences after removing reads with only N: 8,932

Ratio: 
0.89



<br /><br />

### Trim of the read for the primer parts


AlienTrimmer main options: -k 10 -l 30 -m 5 -q 20 -p 0 (Phred+33)  /  26 alien sequence(s)  /  810 k-mers (k=10)
<br />[00:00]       8,932 reads processed:       4,767 trimmed         223 removed
<br /><br />AlienTrimmer also removes reads according to quality criteria

Number of sequence before trimming: 8,932

Number of sequences after trimming: 8,709

Ratio: 
0.98





<br /><br />

###  Read QC n°1


Results are published in the [fastQC1](./fastQC1) folder


Analysis complete for test.fastq2_trim.fq
<br /><br />

### Selection of reads with the attC in 5' and trimming of this sequence





Number of sequences before 5' filtering using ^CAATTCATTCAAGCCGACGCCGCTTCGCGGCGCGGCTTAATTCAAGCG.+$: 8,709

Number of sequences after 5' filtering: 4,391

Ratio: 
0.50



<br /><br />

### Preparation of the graph of base frequencies




Length of the expected attC sequence at the 5' part of reads: 48




Number of bases added after the expected attC sequence at the 5' part of reads, for graphical purpose: 3




Frequencies of the graph are in the [test.fastq2_5pAttc_1-51.stat](./files/test.fastq2_5pAttc_1-51.stat) file








<br /><br />

###  Base frequencies at the 5' extremity of reads




</center>


![Figure 1: Frequency of each base at the 5' of the reads.](./figures/plot_fivep_filtering_stat.png){width=600}


</center>


    
<br /><br />

### Selection of reads over 25 bases




Number of sequences before cutoff at 25 nucleotids: 4,391

Number of sequences after cutoff at 25 nucleotids: 3,742

Ratio: 
0.85






<br /><br />

###  Length of initial reads




</center>


![Figure 2: Frequency of reads according to read size (in bp).](./figures/plot_read_length_ini.png){width=600}


</center>




<br /><br />

###  Length of reads after selection of attC in 5 prime 




</center>


![Figure 3: Frequency of reads according to read size (in bp).](./figures/plot_read_length_fivep_filtering.png){width=600}


</center>




<br /><br />

###  Length of reads after trimming 




</center>


![Figure 4: Frequency of reads according to read size (in bp).](./figures/plot_read_length_fivep_filtering_cut.png){width=600}


</center>




<br /><br />

###  Read length after cut-off




</center>


![Figure 5: Frequency of reads according to read size (in bp).](./figures/plot_read_length_cutoff.png){width=600}


</center>


    


<br /><br />

###  Read QC n°2


Results are published in the [fastQC2](./fastQC2) folder


Analysis complete for test.fastq2_5pAtccRm.fq


<br /><br />

###  Bowtie2 alignment


Time loading reference: 00:00:00
<br />Time loading forward index: 00:00:00
<br />Time loading mirror index: 00:00:00
<br />Multiseed full-index search: 00:00:00
<br />3742 reads; of these:
<br />  3742 (100.00%) were unpaired; of these:
<br />    1240 (33.14%) aligned 0 times
<br />    2308 (61.68%) aligned exactly 1 time
<br />    194 (5.18%) aligned >1 times
<br />66.86% overall alignment rate
<br />Time searching: 00:00:00
<br />Overall time: 00:00:00


<br /><br />

###  MultiQC




Warning: no Kraken performed when using local run



Results are published in the [Report](./reports/multiqc_report.html) folder



<br /><br />

###  Q20 filtering




Number of sequences before Q20 filtering: 3,742



Number of sequences after Q20 filtering: 2,182

Ratio: 
0.58





<br /><br />

###  Control that no more soft clipping in reads


nb of reads with soft clipping (S) in CIGAR: 0


total nb of reads: 2,182
<br /><br />

### Removal of duplicates using the 5\' and 3\' coordinates




Number of sequences before removing duplicates: 2,182

Number of sequences after removing duplicates: 1,660

Ratio: 
0.76






<br /><br />

###  Read coverage




<br /><br />

</center>


![Figure 6: After Bowtie2 alignment](./figures/plot_test.fastq2_bowtie2_mini.png){width=600}


</center>

<br /><br />

</center>


![Figure 7: After Mapping Quality Q20 (1%) filtering](./figures/plot_test.fastq2_q20_dup_mini.png){width=600}


</center>

<br /><br />

</center>


![Figure 8: After removal of reads with identical 5 and 3 coordinates](./figures/plot_test.fastq2_q20_nodup_mini.png){width=600}


</center>


    


<br /><br />

### Insertion positions




One of the step is to recover positions of reverse reads (16), that use the 3' end of the read as insertion site and not the 5' part as with forward reads (0).
It consist in the redefinition of POS according to FLAG in the bam file. See the [insertion_report.txt](./reports/insertion_report.txt) file in the reports folders for details




Number of reads used for insertion computation: 1,660



Number of insertions: 1,660

Ratio: 
1.00









<br /><br />

###  Logos




In each sequence of length 40 <br />position 21 corresponds to the first nucleotide of the reference genome part of the read




<br /><br />

</center>


![Figure 9: test.fastq2_q20_nodup_selected_if_dup_around_insertion_LEADING_16](./figures/logo_test.fastq2_q20_nodup_selected_if_dup_around_insertion_LEADING_16.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 10: test.fastq2_q20_nodup_selected_if_dup_around_insertion_LEADING_0](./figures/logo_test.fastq2_q20_nodup_selected_if_dup_around_insertion_LEADING_0.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 11: test.fastq2_q20_nodup_selected_if_dup_around_insertion_LAGGING_16](./figures/logo_test.fastq2_q20_nodup_selected_if_dup_around_insertion_LAGGING_16.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 12: test.fastq2_q20_nodup_selected_if_dup_around_insertion_LAGGING_0](./figures/logo_test.fastq2_q20_nodup_selected_if_dup_around_insertion_LAGGING_0.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 13: test.fastq2 global logo](./figures/global_logo_nodup_test.fastq2.png){width=600}


</center>


    


<br /><br />

###  Final insertion site files




Warning: in these files, the position indicated is the first nucleotide of the genomic part of the read (the W of the 5'GWT3' consensus site). This means that in FORWARD, the cutting site is before the position. But in REVERSE, the cutting site is after the position.




See the [test.fastq2_q20_nodup_annot.pos](./files/test.fastq2_q20_nodup_annot.pos) and [test.fastq2_q20_nodup_annot.freq](./files/test.fastq2_q20_nodup_annot.freq) files




Number of total positions without duplicates: 1,660



Number of different positions without duplicates: 1,145



<br /><br />

### Motif selected for the random insertions




The forward motif is: G[AT]T




The reverse motif is: A[AT]C




Warning: the position indicated is the first nucleotide of the genomic part of the read (the W of the 5'GWT3' consensus site). This means that in FORWARD, the cutting site is before the position. But in REVERSE, the cutting site is after the position.




Beginning of the motif positions in the forward strand:





```{r, echo = FALSE}
tempo <- read.table('./head.fw.txt', header = TRUE, sep = '\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(head(tempo), row.names = FALSE, caption = NULL, format='html', format.args = list(decimal.mark = '.', big.mark = ',', digits = 0, scientific = 1000)), bootstrap_options = c('striped', 'bordered', 'responsive', 'condensed'), font_size = 10, full_width = FALSE, position = 'left')
```








Beginning of the motif positions in the reverse strand:





```{r, echo = FALSE}
tempo <- read.table('./head.rv.txt', header = TRUE, sep = '\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(head(tempo), row.names = FALSE, caption = NULL, format='html', format.args = list(decimal.mark = '.', big.mark = ',', digits = 0, scientific = 1000)), bootstrap_options = c('striped', 'bordered', 'responsive', 'condensed'), font_size = 10, full_width = FALSE, position = 'left')
```








Number of sites on the forward strand (detected as 5'G[AT]T3' motifs on this strand): 169,209




Number of sites on the reverse strand (detected as 5'A[AT]C3' motifs on the forward strand): 169,139




On the right of the Ori and the left of the ter:
<br />Forward insertion = Leading (i.e., reads with FLAG=0 are leading strand)
<br />Reverse insertion = Lagging (i.e., reads with FLAG=16 are lagging strand)



```{r, echo = FALSE}
tempo <- read.table('./table1.txt', header = TRUE, sep = '\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(head(tempo), row.names = TRUE, caption = NULL, format='html', format.args = list(decimal.mark = '.', big.mark = ',', digits = 0, scientific = 1000)), bootstrap_options = c('striped', 'bordered', 'responsive', 'condensed'), font_size = 10, full_width = FALSE, position = 'left')
```







```{r, echo = FALSE}
tempo <- read.table('./table2.txt', header = TRUE, sep = '\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(head(tempo), row.names = FALSE, caption = NULL, format='html', format.args = list(decimal.mark = '.', big.mark = ',', digits = 0, scientific = 1000)), bootstrap_options = c('striped', 'bordered', 'responsive', 'condensed'), font_size = 10, full_width = FALSE, position = 'left')
```








On the left of the Ori and the right of the ter:
<br />Forward insertion = Lagging (i.e., reads with FLAG=0 are lagging strand)
<br />Reverse insertion = Leading (i.e., reads with FLAG=16 are lagging strand)



```{r, echo = FALSE}
tempo <- read.table('./table3.txt', header = TRUE, sep = '\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(head(tempo), row.names = TRUE, caption = NULL, format='html', format.args = list(decimal.mark = '.', big.mark = ',', digits = 0, scientific = 1000)), c('striped', 'bordered', 'responsive', 'condensed'), font_size = 10, full_width = FALSE, position = 'left')
```







```{r, echo = FALSE}
tempo <- read.table('./table4.txt', header = TRUE, sep = '\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(head(tempo), row.names = FALSE, caption = NULL, format='html', format.args = list(decimal.mark = '.', big.mark = ',', digits = 0, scientific = 1000)), bootstrap_options = c('striped', 'bordered', 'responsive', 'condensed'), font_size = 10, full_width = FALSE, position = 'left')
```








Proportion of the forward leading part of the genome (p.fw.lead = (ter.center - ori.center) / genome_size): 0.497






Proportion of the forward lagging part of the genome (p.fw.lag = 1 - p.fw.lead): 0.503






Proportion of the reverse leading part of the genome (p.rv.lead = p.fw.lag): 0.503






Proportion of the reverse lagging part of the genome (p.rv.lag = p.fw.lead): 0.497






Analysis of biais, using the theo prop based on the genome distances between Ori and ter (theo proportions above divided by 2):



```{r, echo = FALSE}
tempo <- read.table('./table8.txt', header = TRUE, sep = '\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(head(tempo), row.names = FALSE, caption = NULL, format='html', format.args = list(decimal.mark = '.', big.mark = ',', digits = 3, scientific = 1000)), bootstrap_options = c('striped', 'bordered', 'responsive', 'condensed'), font_size = 10, full_width = FALSE, position = 'left')
```






Chi-squared test for given probabilities
<br />
X-squared = 249.66, df = 3, p-value = 7.76e-54




<br /><br />

### Random insertion sites




#### Insertion site counts




See the [random_insertion_report.txt](./reports/random_insertion_report.txt) file for details, notably the number of random sites (which should be the same as the number of observed sites)





<br /><br />

</center>


![Figure 14: Number of motifs insertion per fork](./figures/plot_motif_insertion_per_fork.png){width=400}


</center>




<br /><br />

</center>


![Figure 15: Number of motifs insertion per strand](./figures/plot_motif_insertion_per_strand.png){width=400}


</center>




<br /><br />

</center>


![Figure 16: Number of motifs insertion per strand](./figures/plot_motif_insertion_per_fork_and_strand.png){width=400}


</center>


    


<br /><br />

#### Insertion site proportions





<br /><br />

</center>


![Figure 17: Proportion of motifs insertion per fork](./figures/plot_motif_insertion_per_fork_prop.png){width=500}


<br /><br />

</center>


![Figure 18: Proportion of motifs insertion per strand](./figures/plot_motif_insertion_per_strand_prop.png){width=500}


</center>




<br /><br />

</center>


![Figure 19: Proportion of motifs insertion per strand](./figures/plot_motif_insertion_per_fork_and_strand_prop.png){width=500}


</center>


    


<br /><br />

###  Insertion plots




####  Histograms





</center>


![Figure 20: Insertion site usage (total insertions).](./figures/plot_test.fastq2_insertion_hist_tot.png){width=600}


</center>

<br /><br />


![Figure 21: Insertion site usage zoomed for sites with few insertions (total insertions).](./figures/plot_test.fastq2_insertion_hist_tot_zoom.png){width=600}


</center>

<br /><br />


![Figure 22: Insertion site usage (forward strand).](./figures/plot_test.fastq2_insertion_hist_forward.png){width=600}


</center>

<br /><br />


![Figure 23: Insertion site usage (reverse strand).](./figures/plot_test.fastq2_insertion_hist_reverse.png){width=600}


</center>

<br /><br />


    


<br /><br />

####  Raw frequencies




See the CL Labbook section 24.7.3 to explain the limitation around 100 bp




</center>

<br /><br />


![Figure 24: Raw insertion frequencies.](./figures/plot_test.fastq2_insertion_raw.png){width=600}


</center>

<br /><br />


    


<br /><br />

####  Binned frequencies





<br /><br />

</center>


![Figure 25: frequencies using the binning range of 50,000](./figures/plot_test.fastq2_insertion_bin_50000.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 26: frequencies using the binning range of 50,000](./figures/plot_test.fastq2_lead_lag_insertion_bin_50000.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 27: frequencies using the binning range of 200,000](./figures/plot_test.fastq2_insertion_bin_200000.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 28: frequencies using the binning range of 200,000](./figures/plot_test.fastq2_lead_lag_insertion_bin_200000.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 29: frequencies using the binning range of 500,000](./figures/plot_test.fastq2_insertion_bin_500000.png){width=600}


</center>


        



<br /><br />

</center>


![Figure 30: frequencies using the binning range of 500,000](./figures/plot_test.fastq2_lead_lag_insertion_bin_500000.png){width=600}


</center>


        


<br /><br />

###  Transcription start site (TSS) plots




See the CL Labbook section 48.3 to to get the theoretical proportion of the codant/non codant essential/non essential genome

See the [plot_insertion_report.txt](./reports/plot_insertion_report.txt) file for details about the plotted values.



</center>

<br /><br />


![Figure 31: Promoters per gene.](./figures/plot_test.fastq2_promoter_per_genes.png){width=400}


</center>

<br /><br />


![Figure 32: Distance from TSS.](./figures/hist_test.fastq2_tss_distance_freq.png){width=600}


</center>

<br /><br />


![Figure 33: Distance from TSS and Normal Law.](./figures/hist_test.fastq2_tss_distance_freq_Nlaw.png){width=600}


</center>

<br /><br />


            

<br /><br />

The number of insertions sites are indicated above graphs.


![Figure 34: Insertion relative to TSS.](./figures/boxplot_test.fastq2_tss.png){width=600}


</center>

<br /><br />


![Figure 35: Insertion relative to TSS without unknown.](./figures/boxplot_test.fastq2_tss_wo_unknown.png){width=600}


</center>

<br /><br />


            

<br /><br />

###  Coding sequences (CDS) plots

The number of insertions sites inside CDS are indicated above graphs.

See the [plot_insertion_report.txt](./reports/plot_insertion_report.txt) file for details about the plotted values.


</center>

<br /><br />


![Figure 36: Insertion relative to CDS.](./figures/boxplot_test.fastq2_cds.png){width=600}


</center>

<br /><br />


![Figure 37: Insertion relative to CDS without unknown.](./figures/boxplot_test.fastq2_cds_wo_unknown.png){width=600}


</center>

<br /><br />


            

Warning: the number of observed and random insertions indicated above graphs can be greater than those indicated above Figure 35, since a position that overlaps two genes is counted twice (see the [plot_insertion_report.txt](./reports/plot_insertion_report.txt) file for details).




</center>

<br /><br />


![Figure 38: Insertion per class of CDS.](./figures/barplot_test.fastq2_all.png){width=600}


</center>

<br /><br />


![Figure 39: Kind of insertion relative to CDS.](./figures/barplot_test.fastq2_all_relative.png){width=600}


</center>

<br /><br />


![Figure 40: Kind of insertion relative to CDS.](./figures/barplot_test.fastq2_inside_outside.png){width=600}


</center>

<br /><br />


![Figure 41: Kind of insertion relative to CDS.](./figures/barplot_test.fastq2_ess_uness.png){width=600}


</center>

<br /><br />


        


<br /><br />

###  Analysis with duplicates




See the [test.fastq2_q20_dup_selected_if_dup.pos](./files/test.fastq2_q20_dup_selected_if_dup.pos) and [test.fastq2_q20_dup_annot_selected.freq](./files/test.fastq2_q20_dup_annot_selected.freq) files




Warning: more than the 6 most frequent used sites can be present in the case of frequency equality




Number of total positions using duplicated reads: 2,182



Number of different positions using duplicated reads: 1,145



Number of total positions after selection of the 6 highest used sites: 409



Number of different positions after selection of the 6 highest used sites: 9




<br /><br />

</center>


![Figure 42: With duplicates raw insertion frequencies.](./figures/plot_test.fastq2_insertion_dup_raw.png){width=600}


<br /><br />

</center>


![Figure 43: Selected sites (6 most used insertion sites).](./figures/plot_test.fastq2_insertion_dup_selected.png){width=600}


<br /><br />

</center>


![Figure 44: Insertion site usage (total insertions).](./figures/plot_test.fastq2_insertion_hist_tot_selected.png){width=600}


<br /><br />

</center>


    


Selected sites with frequencies:



```{r, echo = FALSE}
tempo <- read.table('./files/test.fastq2_q20_dup_annot_selected.freq', header = TRUE, colClasses = 'character', sep = '\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(tempo, row.names = TRUE, digits = 0, caption = NULL, format='html'), c('striped', 'bordered', 'responsive', 'condensed'), font_size=10, full_width = FALSE, position = 'left')
```
    


    



<br /><br />

</center>


![Figure 45: Alignment of the selected sites (click [here](./figures/alignment.html) to extend)](./figures/alignment.html){width=600}


</center>


    


<br /><br />

Warning: the frequency of each position is taken into account in the logo plot





<br /><br />

</center>


![Figure 46: With duplicates test.fastq2 global logo on selected sites](./figures/global_logo_dup_test.fastq2.png){width=600}


</center>


    


<br /><br />

###  Backup


See the [reports](./reports) folder for all the details of the analysis, including the parameters used in the .config file


Full .nextflow.log is in: /mnt/c/Users/Gael/Documents/Git_projects/14985_loot<br />The one in the [reports](./reports) folder is not complete (miss the end)


<br /><br />

###  Workflow Version




#### General


| Variable | Value |
| :-- | :-- |
| Project<br />(empty means no .git folder where the main.nf file is present) | loot	https://gitlab.pasteur.fr/gmillot/14985_loot (fetch) | # works only if the main script run is located in a directory that has a .git folder, i.e., that is connected to a distant repo
| Git info<br />(empty means no .git folder where the main.nf file is present) | v8.6.0-dirty | # idem. Provide the small commit number of the script and nextflow.config used in the execution
| Cmd line | nextflow run main.nf -c nextflow.config |
| execution mode | local |
| Manifest's pipeline version | null |
| result path | /mnt/c/Users/Gael/Documents/Git_projects/14985_loot/results/20220120_test_1657729206 |
| nextflow version | 21.04.2 |
    


<br /><br />

#### Implicit variables


| Name | Description | Value | 
| :-- | :-- | :-- |
| launchDir | Directory where the workflow is run | /mnt/c/Users/Gael/Documents/Git_projects/14985_loot |
| nprojectDir | Directory where the main.nf script is located | /mnt/c/Users/Gael/Documents/Git_projects/14985_loot |
| workDir | Directory where tasks temporary files are created | /mnt/c/Users/Gael/Documents/Git_projects/14985_loot/work |
    


<br /><br />

#### User variables


| Name | Description | Value | 
| :-- | :-- | :-- |
| out_path | output folder path | /mnt/c/Users/Gael/Documents/Git_projects/14985_loot/results/20220120_test_1657729206 |
| in_path | input folder path | /mnt/c/Users/Gael/Documents/Git_projects/14985_loot/dataset |
    


<br /><br />

#### Workflow diagram

See the [nf_dag.png](./reports/nf_dag.png) file
