nextflow.enable.dsl=1
/*
#########################################################################
##                                                                     ##
##     main.nf                                                         ##
##     Analysis of integron insertion in procaryotes                   ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################
*/


//////// Arguments of nextflow run

params.modules = ""

//////// end Arguments of nextflow run


//////// Variables

// from the nextflow.config file
config_file = file("${projectDir}/nextflow.config")
log_file = file("${launchDir}/.nextflow.log")
file_name = file("${in_path}/${fastq_file}").baseName
ref_name = file("${ref_path}/${ref_file}").baseName
primer = file(primer_fasta)
ref = file("${ref_path}/${ref_file}")
if(ref_file =~ /Coli|coli|COLI/){
    tss = "${ref_path}/${tss_file}"
    ess = "${ref_path}/${ess_file}"
    cds = "${ref_path}/${cds_file}"
}
sum_of_2_nb = fivep_seq_nb.toInteger()+added_nb.toInteger()
if(system_exec == 'slurm'){
    k2db = file("${kraken_db}")
}
cute_file=file(cute_path)


// end from the nextflow.config file

// from parameters
modules = params.modules // remove the dot -> can be used in bash scripts
// end from parameters

//////// end Variables


//////// Variables from config.file that need to be checked

fastq_ch_test = file("${in_path}/${fastq_file}") // to test if exist below
primer_ch_test = file("${primer_fasta}") // to test if exist below
ref_ch_test = file("${ref_path}/${ref_file}") // to test if exist below
if(ref_file =~ /Coli|coli|COLI/){
    tss_ch_test = file("${ref_path}/${tss_file}")
    ess_ch_test = file("${ref_path}/${ess_file}")
    cds_ch_test = file("${ref_path}/${cds_file}")
}
if(system_exec == 'slurm'){
    k2db_ch_test = file("${kraken_db}") // to test if exist below
}

//////// end Variables from config.file that need to be checked


//////// Channels

fastq_ch = Channel.fromPath("${in_path}/${fastq_file}", checkIfExists: false) // I could use true, but I prefer to perform the check below, in order to have a more explicit error message
grep_pattern_ch = Channel.from("LEADING_0", "LEADING_16", "LAGGING_0", "LAGGING_16")


//////// end Channels

//////// Checks

if(system_exec == 'local' || system_exec == 'slurm' || system_exec == 'slurm_local'){
    def file_exists1 = fastq_ch_test.exists()
    if( ! file_exists1){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID in_path AND fastq_file PARAMETERS IN nextflow.config FILE: ${in_path}/${fastq_file}\n\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
    def file_exists2 = primer_ch_test.exists()
    if( ! file_exists2){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID primer_fasta PARAMETER IN nextflow.config FILE: ${primer_fasta}\n\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
    def file_exists3 = ref_ch_test.exists()
    if( ! file_exists3){
        error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID ref_path: ${ref_path}\nOR ref_file: ${ref_file}\nPARAMETER IN nextflow.config FILE\n\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
    }
    if(ref_file =~ /Coli|coli|COLI/){
        def file_exists4 = tss_ch_test.exists()
        if( ! file_exists4){
            error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID ref_path: ${ref_path}\nOR tss_file: ${tss_file}\nPARAMETER IN nextflow.config FILE\n\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
        }
        def file_exists5 = ess_ch_test.exists()
        if( ! file_exists5){
            error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID ref_path: ${ref_path}\nOR ess_file: ${ess_file}\nPARAMETER IN nextflow.config FILE\n\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
        }
        def file_exists6 = cds_ch_test.exists()
        if( ! file_exists6){
            error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID ref_path: ${ref_path}\nOR cds_file: ${cds_file}\nPARAMETER IN nextflow.config FILE\n\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
        }
    }
    if(system_exec == 'slurm'){
        def file_exists7 = k2db_ch_test.exists()
        if( ! file_exists7){
            error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID kraken_db ${kraken_db}\nPARAMETER IN nextflow.config FILE\n\nIF POINTING TO A DISTANT SERVER, CHECK THAT IT IS MOUNTED\n\n========\n\n"
        }
    }
}else{
    error "\n\n========\n\nERROR IN NEXTFLOW EXECUTION\n\nINVALID system_exec PARAMETER IN nextflow.config FILE: ${system_exec}\nTHE ONLY POSSIBLE VALUES ARE local, slurm OR slurm_local\n\n========\n\n"
}

//////// end Checks



//////// Processes

process init {
    label 'bash' // see the withLabel: bash in the nextflow config file 
    cache 'false'

    output:
    file "report.rmd" into log_ch0

    script:
    """
    echo "---
    title: 'Insertion Sites Report'
    author: 'Gael Millot'
    date: '`r Sys.Date()`'
    output:
      html_document:
        toc: TRUE
        toc_float: TRUE
    ---

    \\n\\n<br /><br />\\n\\n
    " > report.rmd
    """
}
//${projectDir} nextflow variable
//${workflow.commandLine} nextflow variable
//${workflow.manifest.version} nextflow variable
//Note that variables like ${out_path} are interpreted in the script block


process Nremove { // remove the reads made of N only. See section 8.3 of the labbook 20200520
    label 'bash' // see the withLabel: bash in the nextflow config file 
    cache 'true'

    input:
    val file_name
    file gz from fastq_ch

    output:
    file "${file_name}_Nremove.gz" into fastq_Nremove_ch
    file "report.rmd" into log_ch1

    script:
    """
    Nremove.sh ${gz} "${file_name}_Nremove.gz" "report.rmd"
    """
}


process trim { // Trim the oligo sequences. See section 8.4 of the labbook 20200520, the trimming corresponding to section 4 of the labbook 20200505, where we see that reads are trimmed for the blue part of the Primer O6035 (in 5' of the primer). See also the last line of C:\Users\gael\Documents\Git_projects\14985_loot\dataset\20200520_adapters_TruSeq_B2699_14985_CL.fasta
    label 'alien_trimmer' // see the withLabel: bash in the nextflow config file 
    //publishDir "${out_path}", mode: 'copy', pattern: "report.rmd", overwrite: false
    cache 'true'

    input:
    val file_name
    file gz from fastq_Nremove_ch
    file pr from primer
    val alien_l_param from alientrimmer_l_param

    output:
    file "${file_name}_trim.fq" into fastq_trim_ch1, fastq_trim_ch2, fastq_trim_ch3
    file "report.rmd" into log_ch2

    script:
    """
    trim.sh ${gz} "${file_name}_trim.fq" ${pr} ${alien_l_param} "report.rmd"
    """
}

process kraken {
    label 'kraken'

    input:
    file fastq from fastq_trim_ch3
    if(system_exec == 'slurm'){
        val k2db
    }

    output:
    file "${fastq.baseName}_kraken_std.txt" into krakenreports

    script:
    if(system_exec == 'slurm')
        """
        kraken2 --db ${k2db} --threads ${task.cpus} --report ${fastq.baseName}_kraken_std.txt ${fastq} > ${fastq.baseName}.kraken2
        """
    else
        """
        echo "No kraken analysis performed in local running" > ${fastq.baseName}_kraken_std.txt
        """
}


process fastqc1 { // section 8.5 of the labbook 20200520
    label 'fastqc' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/fastQC1", mode: 'copy', pattern: "*_fastqc.*", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    cache 'true'

    input:
    file fq from fastq_trim_ch1

    output:
    file "${fq.baseName}_fastqc.*"
    file "report.rmd" into log_ch3

    script:
    """
    echo -e "\\n\\n<br /><br />\\n\\n###  Read QC n°1\\n\\n" > report.rmd
    echo -e "Results are published in the [fastQC1](./fastQC1) folder\\n\\n" >> report.rmd
    fastqc ${fq} | tee tempo.txt
    cat tempo.txt >> report.rmd
    """
}


process fivep_filtering { // section 8.6 to 8.13 of the labbook 20200520. Instead of analysing and trimming in two steps (29 Nuc of AttC part of the primer then 19 Nuc between primer and Attc cutting site), perform all in a single step, and play with the regex
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/files", mode: 'copy', pattern: "{*.stat,*.length}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    cache 'true'

    input:
    val file_name
    file fq from fastq_trim_ch2
    val fivep_seq_filtering
    val attc_seq
    val fivep_seq_nb
    val added_nb
    val sum_of_2_nb

    output:
    file "${file_name}_5pAtccRm.fq" into fastq_5p_filter_ch1, fastq_5p_filter_ch2
    file "${file_name}_ini.length" into length_fastq_ini_ch
    file "${file_name}_5pAttc.length" into length_fastq_5p_filter_ch
    file "${file_name}_5pAtccRm.stat" into length_fastq_5p_filter_cut_ch
    tuple val("${file_name}_5pAttc_1-${sum_of_2_nb}.stat"), file("${file_name}_5pAttc_1-${sum_of_2_nb}.stat") into stat_fastq_5p_filter_ch1, stat_fastq_5p_filter_ch2
    // Warning: stat_fastq_5p_filter_ch must have the name that finish by "_1-${fivep_seq_nb}.stat". Otherwise, the plot_fivep_filtering process will not work
    file "report.rmd" into log_ch4

    script:
    """
    fivep_filtering.sh ${fq} "${file_name}" "${fivep_seq_filtering}" ${fivep_seq_nb} ${added_nb} ${sum_of_2_nb} "${attc_seq}" "report.rmd"
    """
    // single \n and not \\n because echo "" and not echo ''
    // for nice tables in knitted documents, see https://cran.r-project.org/web/packages/kableExtra/vignettes/awesome_table_in_html.html
}


process plot_fivep_filtering_stat { // section 8.7 to 8.11 of the labbook 20200520
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{*.png}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{plot_fivep_filtering_stat_report.txt}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    cache 'deep'

    input:
    tuple val(nouse), file(stat) from stat_fastq_5p_filter_ch1
    val attc_seq
    file cute_file

    output:
    file "plot_fivep_filtering_stat.png" into fig_ch1
    file "plot_fivep_filtering_stat_report.txt"
    file "report.rmd" into log_ch5
    val "sq" into sq_ch
    // sq is the sequence that will be used as x-axis for plot_fivep_filtering_stat

    script:
    """
    echo -e "
\\n\\n<br /><br />\\n\\n###  Base frequencies at the 5\' extremity of reads\\n\\n
\\n\\n</center>\\n\\n
![Figure 1: Frequency of each base at the 5\' of the reads.](./figures/plot_fivep_filtering_stat.png){width=600}
\\n\\n</center>\\n\\n
    " > report.rmd
    plot_fivep_filtering_stat.R "${stat}" "${attc_seq}" "${cute_file}" "plot_fivep_filtering_stat_report.txt"
    """
    // not space before <center> or # ![. Otherwise not correctly interpreted
    // single quotes required because of the !
    // Warning: $workflow.projectDir/bin/ is the only way to have the execution rights of a .R file in the bin directory when the gitlab repo is pulled into /pasteur/sonic/homes/gmillot/.nextflow/assets/. See https://github.com/nextflow-io/nextflow/issues/698. Otherwise, the following message can appear: Fatal error: cannot open file '/pasteur/sonic/homes/gmillot/.nextflow/assets/gmillot/14985_loot/bin/plot_fivep_filtering.R': No such file or directory
}


process cutoff { // section 8.16 of the labbook 20200520
    label 'bash' // see the withLabel: bash in the nextflow config file 
    cache 'true'

    input:
    val file_name
    file fq from fastq_5p_filter_ch2
    val nb from cutoff_nb

    output:
    file "${file_name}_cutoff.fq" into cutoff_ch
    file "${file_name}_cutoff.length" into length_cutoff_ch
    file "report.rmd" into log_ch6

    script:
    """
    cutoff.sh ${fq} ${nb} "${file_name}" "report.rmd"
    """
}


process plot_read_length { // section 8.8 section 8.12 of the labbook 20200520
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{*.png}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{plot_read_length_report.txt}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    cache 'true'

    input:
    file length2 from length_fastq_ini_ch
    file length3 from length_fastq_5p_filter_ch
    file length4 from length_fastq_5p_filter_cut_ch
    file length5 from length_cutoff_ch
    file cute_file

    output:
    file "*.png" into fig_ch2 // warning: several files
    file "plot_read_length_report.txt"
    file "report.rmd" into log_ch7

    script:
    """
    echo -e '
\\n\\n<br /><br />\\n\\n###  Length of initial reads\\n\\n
\\n\\n</center>\\n\\n
![Figure 2: Frequency of reads according to read size (in bp).](./figures/plot_read_length_ini.png){width=600}
\\n\\n</center>\\n\\n
\\n\\n<br /><br />\\n\\n###  Length of reads after selection of attC in 5 prime \\n\\n
\\n\\n</center>\\n\\n
![Figure 3: Frequency of reads according to read size (in bp).](./figures/plot_read_length_fivep_filtering.png){width=600}
\\n\\n</center>\\n\\n
\\n\\n<br /><br />\\n\\n###  Length of reads after trimming \\n\\n
\\n\\n</center>\\n\\n
![Figure 4: Frequency of reads according to read size (in bp).](./figures/plot_read_length_fivep_filtering_cut.png){width=600}
\\n\\n</center>\\n\\n
\\n\\n<br /><br />\\n\\n###  Read length after cut-off\\n\\n
\\n\\n</center>\\n\\n
![Figure 5: Frequency of reads according to read size (in bp).](./figures/plot_read_length_cutoff.png){width=600}
\\n\\n</center>\\n\\n
    ' > report.rmd
    plot_read_length.R "${length2}" "${length3}" "${length4}" "${length5}" "${cute_file}" "plot_read_length_report.txt"
    """
    // single quotes required because of the !
}


process fastqc2 { // section 8.14 of the labbook 20200520
    label 'fastqc' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/fastQC2", mode: 'copy', pattern: "*_fastqc.*", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    cache 'true'

    input:
    file fq from fastq_5p_filter_ch1

    output:
    file "${fq.baseName}_fastqc.*"
    path "*_fastqc.zip" into fastqc_log_ch
    file "report.rmd" into log_ch8

    script:
    """
    echo -e "\\n\\n<br /><br />\\n\\n###  Read QC n°2\\n\\n" > report.rmd
    echo -e "Results are published in the [fastQC2](./fastQC2) folder\\n\\n" >> report.rmd
    fastqc ${fq} | tee tempo.txt
    cat tempo.txt >> report.rmd
    """
}


process bowtie2 { // section 24.1 of the labbook 20200707
    label 'bowtie2' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/reports", mode: 'copy', pattern: "bowtie2_report.txt", overwrite: false //
    // publishDir "${out_path}/files", mode: 'copy', pattern: "${file_name}_bowtie2.bam", overwrite: false // 
    cache 'true'

    input:
    val file_name
    val ref_name
    file fq from cutoff_ch
    file ref

    output:
    file "${file_name}_bowtie2.bam" into bowtie2_ch1, bowtie2_ch2
    file "bowtie2_report.txt" into bowtie2_log_ch
    file "report.rmd" into log_ch9

    script:
    """
    echo -e "\\n\\n<br /><br />\\n\\n###  Bowtie2 indexing of the reference sequence\\n\\n" >> bowtie2_report.txt
    bowtie2-build ${ref} ${ref_name} |& tee -a bowtie2_report.txt
    echo -e "\\n\\n<br /><br />\\n\\n###  Bowtie2 alignment\\n\\n" > report.rmd
    echo -e "\\n\\n<br /><br />\\n\\n###  Bowtie2 alignment\\n\\n" >> bowtie2_report.txt
    bowtie2 --very-sensitive -x ${ref_name} -U ${fq} -t -S ${file_name}_bowtie2.sam |& tee -a tempo.txt
    # --very-sensitive: no soft clipping allowed and very sensitive seed alignment
    # -t time displayed
    cat tempo.txt >> bowtie2_report.txt
    sed -i -e ':a;N;\$!ba;s/\\n/\\n<br \\/>/g' tempo.txt
    cat tempo.txt >> report.rmd
    echo -e "\\n\\n<br /><br />\\n\\n####  samtools conversion\\n\\n" >> bowtie2_report.txt
    # samtools faidx ${ref}
    samtools view -bh -o tempo.bam ${file_name}_bowtie2.sam |& tee -a bowtie2_report.txt
    samtools sort -o ${file_name}_bowtie2.bam tempo.bam |& tee -a bowtie2_report.txt
    samtools index ${file_name}_bowtie2.bam |& tee -a bowtie2_report.txt
    """
}


process multiQC{
    label "multiqc"
    publishDir "${out_path}/reports", mode: 'copy', pattern: "multiqc_report.html", overwrite: false

    input:
    file "*" from fastqc_log_ch.mix(bowtie2_log_ch).mix(krakenreports).collect()

    output:
    file "multiqc_report.html" into multiqc_ch
    file "report.rmd" into log_ch10

    script:
    """
    multiqc . -n multiqc_report.html
    echo -e "\\n\\n<br /><br />\\n\\n###  MultiQC\\n\\n" > report.rmd
    if [[ ${system_exec} == "local" ]] ; then
        echo -e "\\n\\nWarning: no Kraken performed when using local run\\n" >> report.rmd
    fi
    echo -e "\\n\\nResults are published in the [Report](./reports/multiqc_report.html) folder\\n" >> report.rmd
    """
}



process Q20 { // section 24.2 of the labbook 20200707
    label 'samtools' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/reports", mode: 'copy', pattern: "q20_report.txt", overwrite: false
    // publishDir "${out_path}/files", mode: 'copy', pattern: "${file_name}_q20.bam", overwrite: false // 
    cache 'true'

    input:
    val file_name
    file bam from bowtie2_ch1

    output:
    file "${file_name}_q20_dup.bam" into q20_ch1, q20_ch2, q20_ch3, q20_ch4
    file "read_nb_before" into bow_read_nb_ch
    file "read_nb_after" into q20_read_nb_ch
    file "q20_report.txt"
    file "report.rmd" into log_ch11

    script:
    """
    samtools view -q 20 -b ${bam} > ${file_name}_q20_dup.bam |& tee q20_report.txt
    samtools index ${file_name}_q20_dup.bam
    echo -e "\\n\\n<br /><br />\\n\\n###  Q20 filtering\\n\\n" > report.rmd
    read_nb_before=\$(samtools view ${bam} | wc -l | cut -f1 -d' ') # -h to add the header
    read_nb_after=\$(samtools view ${file_name}_q20_dup.bam | wc -l | cut -f1 -d' ') # -h to add the header
    echo -e "\\n\\nNumber of sequences before Q20 filtering: \$(printf "%'d" \${read_nb_before})\\n" >> report.rmd
    echo -e "\\n\\nNumber of sequences after Q20 filtering: \$(printf "%'d" \${read_nb_after})\\n" >> report.rmd
    echo -e "Ratio: " >> report.rmd
    echo -e \$(printf "%.2f\n" \$(echo \$" \$read_nb_after / \$read_nb_before " | bc -l)) >> report.rmd # the number in '%.2f\\n' is the number of decimals
    echo -e "\\n\\n" >> report.rmd
    echo \$read_nb_before > read_nb_before # because nf cannot output values easily
    echo \$read_nb_after > read_nb_after
    """
}


process no_soft_clipping { // section 24.4 of the labbook 20200707
    label 'samtools' // see the withLabel: bash in the nextflow config file 
    cache 'true'

    input:
    file bam from q20_ch1

    output:
    file "report.rmd" into log_ch12

    script:
    """
    echo -e "\\n\\n<br /><br />\\n\\n###  Control that no more soft clipping in reads\\n\\n" > report.rmd
    echo -e "nb of reads with soft clipping (S) in CIGAR: \$(printf "%'d" \$(samtools view ${bam} | awk '\$6 ~ /.*[S].*/{print \$0}' | wc -l | cut -f1 -d' '))" >> report.rmd
    echo -e "\\n\\ntotal nb of reads: \$(printf "%'d" \$(samtools view ${bam} | wc -l | cut -f1 -d' '))" >> report.rmd
    """
}


process duplicate_removal { // section 24.5 of the labbook 20200707. Warning: USING 5' AND 3' COORDINATES
    label 'samtools' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/reports", mode: 'copy', pattern: "dup_report.txt", overwrite: false
    // publishDir "${out_path}/files", mode: 'copy', pattern: "${file_name}_q20_nodup.bam", overwrite: false // 
    cache 'true'

    input:
    val file_name
    file bam from q20_ch2
    file ref

    output:
    file "${file_name}_q20_nodup.bam" into dup_ch1, dup_ch2
    file "dup_read_nb" into dup_read_nb_ch
    file "dup_report.txt"
    file "report.rmd" into log_ch13

    script:
    """
    duplicate_removal.sh ${bam} ${ref} "${file_name}_q20_nodup.bam" "dup_report.txt" "report.rmd"
    """
}

// process picard_duplicates { // section 24.3 of the labbook 20200707


process report1 {
    label 'bash'
    cache 'true'

    input:
    val file_name

    output:
    file "report.rmd" into log_ch14

    script:
    """
    echo -e '
\\n\\n<br /><br />\\n\\n###  Read coverage\\n\\n
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 6: After Bowtie2 alignment](./figures/plot_${file_name}_bowtie2_mini.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 7: After Mapping Quality Q20 (1%) filtering](./figures/plot_${file_name}_q20_dup_mini.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 8: After removal of reads with identical 5\' and 3\' coordinates](./figures/plot_${file_name}_q20_nodup_mini.png){width=600}
\\n\\n</center>\\n\\n
    ' > report.rmd
    """
}


process coverage { // section 24.5 of the labbook 20200707. Warning: USING 5' AND 3' COORDINATES
    label 'bedtools' // see the withLabel: bash in the nextflow config file 
    // publishDir "${out_path}/reports", mode: 'copy', pattern: "cov_report.txt", overwrite: false // inactivated because no cov_report published in "${out_path}/reports" probably because of the parallelization
    publishDir "${out_path}/files", mode: 'copy', pattern: "*.cov", overwrite: false
    cache 'true'

    input:
    file bam from bowtie2_ch2.concat(q20_ch3, dup_ch1)
    // file ref from ref_ch3 // not required because bedtools genomecov-g ${ref} not required when inputs are bam files

    output:
    file "*_mini.cov" into cov_ch // warning: 3 files
    // file "*.cov" // coverage per base if ever required but long process
    file "cov_report.txt" into cov_report_ch

    script:
    """
    # bedtools genomecov -d -ibam \${bam} > \${bam.baseName}.cov |& tee cov_report.txt # coverage per base if ever required but long process
    # to add the chr names | awk '{h[\$NF]++}; END { for(k in h) print k, h[k] }' | sort -V > \${bam.baseName}.cov
    bedtools genomecov -bga -ibam ${bam}  > ${bam.baseName}_mini.cov |& tee cov_report.txt
    # -g \${ref} not required when inputs are bam files
    """
}


cov_report_ch.collectFile(name: "cov_report.txt").subscribe{it -> it.copyTo("${out_path}/reports")} // concatenate all the cov_report.txt files in channel cov_report_ch into a single file published into 



process plot_coverage { // section 24.6 of the labbook 20200707
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{*.png}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    // publishDir "${out_path}/reports", mode: 'copy', pattern: "{plot_coverage_report.txt}", overwrite: false // 
    cache 'true'

    input:
    val file_name
    file cov from cov_ch // warning: 3 files
    file read_nb from bow_read_nb_ch.concat(q20_read_nb_ch, dup_read_nb_ch)
    val ori_coord
    val ter_coord
    val color_coverage
    val xlab
    file cute_file

    output:
    file "plot_${cov.baseName}.png" into fig_ch3 // warning: several files
    file "plot_coverage_report.txt" into plot_cov_report_ch

    script:
    """
    plot_coverage.R "${cov.baseName}" "${read_nb}" "${ori_coord}" "${ter_coord}" "${color_coverage}" "${xlab}" "${file_name}" "${cute_file}" "plot_coverage_report.txt"
    """
    // single quotes required because of the !
}


plot_cov_report_ch.collectFile(name: "plot_cov_report.txt").subscribe{it -> it.copyTo("${out_path}/reports")} // concatenate all the cov_report.txt files in channel cov_report_ch into a single file published into ${out_path}/reports


// From here, treatment of the bam with and without duplicates
process insertion { // section 24.7 of the labbook 20200707
    label 'samtools' // see the withLabel: bash in the nextflow config file 
    // publishDir "${out_path}/reports", mode: 'copy', pattern: "*_insertion_report.txt", overwrite: false
    publishDir "${out_path}/files", mode: 'copy', pattern: "*.pos", overwrite: false // 
    cache 'true'

    input:
    val file_name
    file bam from dup_ch2.concat(q20_ch4) // warning: 2 files (no duplication and duplications) -> parallelization

    output:
    file "${bam.baseName}.pos" into orient_ch1 // warning: 2 files (no duplication and duplications)
    file "insertion_report.txt" into insertion_report_ch // warning: several files
    file "report.rmd" into tempo_log_ch // optional true into log_ch15 // single file
    // optional true: report.rmd sent into a channel only if the process has generated a report.rmd. Of note, if("${bam}" == "${file_name}_q20_nodup.bam"){file "report.rmd" into log_ch15} does not work

    script:
    """
    if [[ ${bam} == "${file_name}_q20_nodup.bam" ]] ; then
        echo -e "\\n\\n<br /><br />\\n\\n### Insertion positions\\n\\n" > report.rmd
        echo -e "\\n\\nOne of the step is to recover positions of reverse reads (16), that use the 3' end of the read as insertion site and not the 5' part as with forward reads (0).\\nIt consist in the redefinition of POS according to FLAG in the bam file. See the [insertion_report.txt](./reports/insertion_report.txt) file in the reports folders for details\\n\\n" >> report.rmd
    fi

    # extraction of bam column 2, 4 and 10, i.e., FLAG, POS and SEQ
    samtools view ${bam} | awk 'BEGIN{FS="\\t" ; OFS="" ; ORS=""}{print ">"\$2"\\t"\$4"\\n"\$10"\\n" }' > tempo
    # Of note, samtools fasta \$DIR/\$SAMPLE_NAME > \${OUTPUT}.fasta # convert bam into fasta
    echo -e "\\n\\n######## ${bam} file\\n\\n" > insertion_report.txt
    cat tempo | head -60 | tail -20 >> insertion_report.txt
    echo -e "\\n\\nExtraction of the FLAG (containing the read orientation) the POS and the SEQ of the bams\\nHeader is the 1) sens of insersion (0 or 16) and 2) insertion site position\\n\\n" >> insertion_report.txt
    # redefinition of POS according to FLAG
    awk 'BEGIN{FS="\t" ; OFS="" ; ORS=""}{lineKind=(NR-1)%2}lineKind==0{orient=(\$1~">16") ; if(orient){var1 = \$1 ; var2 = \$2}else{print \$0"\\n"}}lineKind==1{if(orient){var3 = length(\$0) ; var4 = var2 + var3 - 1 ; print var1"\\t"var4"\\n"\$0"\\n"}else{print \$0"\\n"}}' tempo > ${file_name}_reorient.fasta
    echo -e "\\n\\nFinal fasta file\\n\\nPositions of reverse reads (16) use the 3' end of the read as insertion site and not the 5' part as with forward reads (0)\\n\\n" >> insertion_report.txt
    cat ${file_name}_reorient.fasta | head -60 | tail -20 >> insertion_report.txt
    awk '{lineKind=(NR-1)%2}lineKind==0{gsub(/>/, "", \$1) ; print \$0}' ${file_name}_reorient.fasta > ${bam.baseName}.pos
    echo -e "\\n\\nFinal pos file\\n\\n" >> insertion_report.txt
    cat ${bam.baseName}.pos | head -60 | tail -20 >> insertion_report.txt

    read_nb_before=\$(samtools view ${bam} | wc -l | cut -f1 -d' ') # -h to add the header. Thus do not put here
    read_nb_after=\$(wc -l ${bam.baseName}.pos | cut -f1 -d' ')
    if [[ ${bam} == "${file_name}_q20_nodup.bam" ]] ; then
        echo -e "\\n\\nNumber of reads used for insertion computation: \$(printf "%'d" \${read_nb_before})\\n" >> report.rmd
        echo -e "\\n\\nNumber of insertions: \$(printf "%'d" \${read_nb_after})\\n" >> report.rmd
        echo -e "Ratio: " >> report.rmd
        echo -e \$(printf "%.2f\n" \$(echo \$" \$read_nb_after / \$read_nb_before " | bc -l)) >> report.rmd # the number in '%.2f\\n' is the number of decimals
        echo -e "\\n\\n" >> report.rmd
    else
        echo -e "\\n\\n" >> report.rmd
    fi
    """
}

tempo_log_ch.collectFile(name: "report.rmd").set{log_ch15}
insertion_report_ch.collectFile(name: "insertion_report.txt").subscribe{it -> it.copyTo("${out_path}/reports")} // concatenate all the cov_report.txt files in channel cov_report_ch into a single file published into ${out_path}/reports


process final_insertion_files { // 44.1 of the labbook 20201210. Also select the nb_max_insertion_sites most frequent sites in the duplicated file
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/files", mode: 'copy', pattern: "{*.pos,*.freq}", overwrite: false
    cache 'true'

    input:
    file pos from orient_ch1 // warning: 2 files
    val ori_coord
    val ter_coord
    file cute_file
    val nb_max_insertion_sites

    output:
    file "${pos.baseName}_selected_if_dup.pos" into pos_ch1, pos_ch2 // warning: 2 files, selected sites if duplicated
    file "${pos.baseName}_annot.pos" into pos_annot_ch // warning: 2 files, non selected sites if duplicated
    file "${pos.baseName}_annot.freq" into freq_ch // warning: 2 files, non selected sites if duplicated
    file "${pos.baseName}_annot_selected.freq" into freq_dup_selected_ch // optional true into freq_dup_selected_ch1, freq_dup_selected_ch2 // warning: 1 files, optional true because only made with dup file
    file "final_insertion_files_report.txt" into final_insertion_files_report_ch // warning: 2 files

    script:
    """
    final_insertion_files.R "${pos}" "${ori_coord}" "${ter_coord}" "${nb_max_insertion_sites}" "${pos.baseName}" "${cute_file}" "final_insertion_files_report.txt"
    """
}

pos_ch1.branch{
    nodup: it.getName() =~ /nodup/
    dup: true
}.set{tempo_ch2}
tempo_ch2.dup.set{pos_dup_selected_ch}

pos_annot_ch.branch{
    nodup: it.getName() =~ /nodup/
    dup: true
}.set{tempo_ch2}
tempo_ch2.nodup.into{pos_nodup_ch1 ; pos_nodup_ch2}
tempo_ch2.dup.set{pos_dup_nonselected_ch}

freq_ch.branch{
    nodup: it.getName() =~ /nodup/
    dup: true
}.set{tempo_ch2}
tempo_ch2.dup.set{freq_dup_ch1} // warning: 1 file now

freq_dup_selected_ch.branch{
    nodup: it.getName() =~ /nodup/
    dup: true
}.set{tempo_ch2}
tempo_ch2.dup.into{freq_dup_selected_ch1 ; freq_dup_selected_ch2} // warning: 1 file now

final_insertion_files_report_ch.collectFile(name: "final_insertion_files_report.txt").subscribe{it -> it.copyTo("${out_path}/reports")}


process seq_around_insertion { // sections 24.9.1 of the labbook 20200707
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/files", mode: 'copy', pattern: "{*.bed}", overwrite: false
    // publishDir "${out_path}/reports", mode: 'copy', pattern: "seq_around_insertion_report.txt", overwrite: false
    cache 'true'

    input:
    val file_name
    file pos from pos_ch2 // warning: 2 files
    val ori_coord
    val ter_coord
    val insertion_dist
    file cute_file

    output:
    file "${pos.baseName}_around_insertion.bed" into around_insertion_bed_ch // warning: 2 files
    file "seq_around_insertion_report.txt" into seq_around_insertion_report_ch // warning: 2 files

    script:
    """
    seq_around_insertion.R "${pos}" "${ori_coord}" "${ter_coord}" "${insertion_dist}" "${pos.baseName}" "${cute_file}" "seq_around_insertion_report.txt"
    """
}

seq_around_insertion_report_ch.collectFile(name: "seq_around_insertion_report.txt").subscribe{it -> it.copyTo("${out_path}/reports")} // concatenate all the cov_report.txt files in channel cov_report_ch into a single file published into ${out_path}/reports

process extract_seq { // section 24.9.2 of the labbook 20200707
    // Warning: the fasta is always taken on the watson (top) strand. The -s option need a special bed file. See https://bedtools.readthedocs.io/en/latest/content/tools/getfasta.html
    label 'bedtools'
    publishDir "${out_path}/files", mode: 'copy', pattern: "${bed.baseName}.fasta", overwrite: false
    // publishDir "${out_path}/reports", mode: 'copy', pattern: "extract_seq_report.txt"
    cache 'true'

    input:
    file bed from around_insertion_bed_ch // warning: 2 files
    file ref

    output:
    file "${bed.baseName}.fasta" into extract_seq_ch // warning: 2 files
    file "extract_seq_report.txt" into extract_seq_report_ch // warning: 2 files

    script:
    """
    echo -e "\n\n######## ${bed} file\n\n" > extract_seq_report.txt
    # make a bed file from the reference genome
    echo ">ref" > tempo.ref.fasta
    awk '{lineKind=(NR-1)%2}lineKind==1{print \$0}' ${ref} >> tempo.ref.fasta |& tee extract_seq_report.txt
    bedtools getfasta -fi tempo.ref.fasta -bed ${bed} -fo "${bed.baseName}.fasta" -name |& tee extract_seq_report.txt
    rm tempo.ref.fasta
    rm tempo.ref.fasta.fai
    """
}

// splitting of the extract_seq_ch channel
extract_seq_ch.branch{
    nodup: it.getName() =~ /nodup/
    dup: true
}.set{tempo_ch1}
tempo_ch1.nodup.set{extract_seq_nodup_ch1} // warning: 1 files
tempo_ch1.dup.into{extract_seq_dup_ch1 ; extract_seq_dup_ch2} // warning: 1 files
 // the 2 files in channel extract_seq_ch are splitted into two sub channel nodup and dup, according to nodup regex present in the name of the 2 files. See https://www.nextflow.io/docs/latest/operator.html?highlight=branch
// end splitting of the extract_seq_ch channel

extract_seq_report_ch.collectFile(name: "extract_seq_report.txt").subscribe{it -> it.copyTo("${out_path}/reports")} // concatenate all the cov_report.txt files in channel cov_report_ch into a single file published into ${out_path}/reports


process base_freq { // section 24.9.2 of the labbook 20200707
    // parallelization, i.e., computed for each forward, reverse, leadding or lagging seq from the fasta file
    label 'bash'
    publishDir "${out_path}/files", mode: 'copy', pattern: "{*.seq,*.stat}", overwrite: false
    // publishDir "${out_path}/reports", mode: 'copy', pattern: "base_freq_report.txt"
    cache 'true'

    input:
    val file_name
    tuple val(grep_pattern), file(fasta) from grep_pattern_ch.combine(extract_seq_nodup_ch1.concat(extract_seq_dup_ch1)) // warning: 4 values and 2 files respectively -> 8 parall expected
    //file fasta from extract_seq_ch // warning: several files
    file ref

    output:
    file "${fasta.baseName}_${grep_pattern}.seq"
    file "${fasta.baseName}_${grep_pattern}.stat" into base_freq_stat_ch // warning: 8 files
    file "base_freq_report.txt" into base_freq_report_ch

    script:
    """
    # file splitting into seq
    awk -v var1=${grep_pattern} '
        {lineKind=(NR-1)%2}
        lineKind==0{toGet=(\$0 ~ ">" var1) ; next}
        lineKind==1{if(toGet){print \$0}}
        ' ${fasta} > ${fasta.baseName}_${grep_pattern}.seq |& tee base_freq_report.txt
    # ATGC contingency
    gawk '{
        L=length(\$0);
        for(i=1;i<=L;i++) {
            B=substr(\$0,i,1);
            T[i][B]++;
        }
      }
      END{
            for(BI=0;BI<5;BI++) {
                B=(BI==0?"A":(BI==1?"C":(BI==2?"G":(BI==3?"T":"Other"))));
                printf("%s",B); 
                for(i in T) {
                    tot=0.0;
                    for(B2 in T[i]){
                        tot+=T[i][B2];
                    }
                printf("\t%0.5f",(T[i][B])); # T[i][B]/tot for proportion
                } 
            printf("\\n");
            }
    }' ${fasta.baseName}_${grep_pattern}.seq > ${fasta.baseName}_${grep_pattern}.stat |& tee base_freq_report.txt
    """
}

// splitting of the base_freq_stat_ch channel
base_freq_stat_ch.branch{
    nodup: it.getName() =~ /nodup/
    dup: true
}.set{tempo_ch2}
tempo_ch2.nodup.into{base_freq_stat_nodup_ch1 ; base_freq_stat_nodup_ch2 ; base_freq_stat_nodup_ch3} // warning: 4 files
tempo_ch2.dup.set{base_freq_stat_dup_ch1} // warning: 4 files
 // the 8 files in channel base_freq_stat_ch are splitted into two sub channel nodup and dup, according to nodup regex present in the name of the 8 files. See https://www.nextflow.io/docs/latest/operator.html?highlight=branch
// end splitting of the base_freq_stat_ch channel

base_freq_report_ch.collectFile(name: "base_freq_report.txt").subscribe{it -> it.copyTo("${out_path}/reports")} // concatenate all the cov_report.txt files in channel cov_report_ch into a single file published into ${out_path}/reports


process report2 {
    label 'bash'
    cache 'true'

    input:
    val files from base_freq_stat_nodup_ch1.collect() // warning: 4 files. Collect() prevent the parallelization
    val insertion_dist

    output:
    file "report.rmd" into log_ch16

    script:
    """
    echo -e "
\\n\\n<br /><br />\\n\\n###  Logos\\n\\n
\\n\\nIn each sequence of length \$((${insertion_dist} * 2)) <br />position \$((${insertion_dist} + 1)) corresponds to the first nucleotide of the reference genome part of the read
" > report.rmd
    count=0 # always goes to 4 because 4 figures, one for each forward/reverse leading/lagging
    for i in \$(echo ${files.baseName} | sed 's/^\\[//' | sed 's/\\]\$//' | sed 's/,//g') ; do
        echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure \'\$((9 + \$count))\': \'\${i}\'](./figures/logo_\'\${i}\'.png){width=600}
\\n\\n</center>\\n\\n
        ' >> report.rmd
        count=\$((count + 1))
    done
    echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 13: ${file_name} global logo](./figures/global_logo_nodup_${file_name}.png){width=600}
\\n\\n</center>\\n\\n
    ' >> report.rmd
    """
}


process logo { // 24.9.3 of the labbook 20200707
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{*.png}", overwrite: false
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{logo_report.txt}", overwrite: false // 
    cache 'true'

    input:
    file freq from base_freq_stat_nodup_ch2 // warning: 4 files
    val insertion_dist
    file cute_file

    output:
    file "*.png" into fig_ch4 // warning: 4 files
    file "logo_report.txt"

    script:
    """
    logo.R "${freq}" ${insertion_dist} "${cute_file}" "logo_report.txt"
    """
    // single quotes required because of the !
}

process global_logo { // 24.9.3 of the labbook 20200707
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{*.png}", overwrite: false
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{global_logo_report.txt}", overwrite: false // 
    cache 'true'

    input:
    val file_name
    file freq from base_freq_stat_nodup_ch3.collect().concat(base_freq_stat_dup_ch1.collect()) // warning: 4 files. Collect() prevent the parallelization and concat() associate -> 2 parallelization expected
    val insertion_dist
    file cute_file

    output:
    file "*.png" into fig_ch5 // 2 files
    file "global_logo_report.txt"

    script:
    """
    global_logo.R "${freq}" "${file_name}" "${insertion_dist}" "${cute_file}" "global_logo_report.txt"
    """
    // single quotes required because of the !
}



process report3 {
    label 'bash'
    cache 'true'

    input:
    file pos from pos_nodup_ch1

    output:
    file "report.rmd" into log_ch17

    script:
    """

    echo -e "\\n\\n<br /><br />\\n\\n###  Final insertion site files\\n\\n" > report.rmd
    echo -e "\\n\\nWarning: in these files, the position indicated is the first nucleotide of the genomic part of the read (the W of the 5'GWT3' consensus site). This means that in FORWARD, the cutting site is before the position. But in REVERSE, the cutting site is after the position.\\n\\n" >> report.rmd
    echo -e "\\n\\nSee the [${pos.baseName}.pos](./files/${pos.baseName}.pos) and [${pos.baseName}.freq](./files/${pos.baseName}.freq) files\\n\\n" >> report.rmd
    pos_nb=\$(( \$(wc -l ${pos} | cut -f1 -d' ') - 1)) # -1 because first line is the header
    pos_uniq_nb=\$(( \$(sort -u ${pos} | wc -l | cut -f1 -d' ') - 1)) # -1 because first line is the header
    echo -e "\\n\\nNumber of total positions without duplicates: \$(printf "%'d" \${pos_nb})\\n" >> report.rmd
    echo -e "\\n\\nNumber of different positions without duplicates: \$(printf "%'d" \${pos_uniq_nb})\\n" >> report.rmd
    """
}




process motif { // 43 of the labbook 20201209
    // for random insertions
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/files", mode: 'copy', pattern: "motif_sites.pos", overwrite: false
    publishDir "${out_path}/reports", mode: 'copy', pattern: "motif_report.txt", overwrite: false
    cache 'true'

    input:
    val motif_fw
    val motif_rev
    file ref
    val ori_coord
    val ter_coord
    val genome_size
    file cute_file

    output:
    file "motif_sites.pos" into motif_ch
    file "motif_report.txt"
    file "{head,table}*.txt" into motif_table_ch // warning: several files
    file "report.rmd" into log_ch18

    script:
    """
    echo -e "\\n\\n<br /><br />\\n\\n### Motif selected for the random insertions\\n\\n" > report.rmd
    echo -e "\\n\\nThe forward motif is: ${motif_fw}\\n\\n" >> report.rmd
    echo -e "\\n\\nThe reverse motif is: ${motif_rev}\\n\\n" >> report.rmd
    if [[ ${motif_fw} != "NULL" ]] ; then
        cat ${ref} | sed '1d' | grep -Ebo '${motif_fw}' > motif_fw.pos
        cat ${ref} | sed '1d' | grep -Ebo '${motif_rev}' > motif_rev.pos
    else
        cat ${ref} | sed '1d' | grep -Ebo '[ACGT]' > motif_fw.pos
        cat ${ref} | sed '1d' | grep -Ebo '[ACGT]' > motif_rev.pos
    fi
    echo -e "\\n\\nWarning: the position indicated is the first nucleotide of the genomic part of the read (the W of the 5'GWT3' consensus site). This means that in FORWARD, the cutting site is before the position. But in REVERSE, the cutting site is after the position.\\n\\n" >> report.rmd
    motif.R "motif_fw.pos" "motif_rev.pos" "${ori_coord}" "${ter_coord}" "${genome_size}" "${motif_fw}" "${motif_rev}" "${cute_file}" "motif_report.txt" "report.rmd"

    """
}


process random_insertion { // sections 44 of the labbook 20201210
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{*.png}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    publishDir "${out_path}/files", mode: 'copy', pattern: "{*.pos,*.freq}", overwrite: false
    publishDir "${out_path}/reports", mode: 'copy', pattern: "random_insertion_report.txt", overwrite: false
    cache 'true'

    input:
    val file_name
    file pos from pos_nodup_ch2 // a single file now here
    file motif_pos from motif_ch
    val ori_coord
    val ter_coord
    val motif_fw
    val genome_size
    file cute_file

    output:
    file "*.png" into fig_ch6 // warning: 6 files
    file "obs_rd_insertions.pos" into obs_rd_insertions_pos_ch1
    file "obs_rd_insertions.freq" into obs_rd_insertions_freq_ch1
    file "random_insertion_report.txt"
    file "report.rmd" into log_ch19

    script:
    """
    random_insertion.R "${pos}" "${motif_pos}" "${ori_coord}" "${ter_coord}" "${motif_fw}" "${genome_size}" "${file_name}" "${cute_file}" "random_insertion_report.txt"

    echo -e "\\n\\n<br /><br />\\n\\n### Random insertion sites\\n\\n" > report.rmd
    echo -e "\\n\\n#### Insertion site counts\\n\\n" >> report.rmd
    echo -e "\\n\\nSee the [random_insertion_report.txt](./reports/random_insertion_report.txt) file for details, notably the number of random sites (which should be the same as the number of observed sites)\\n\\n" >> report.rmd
    echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 14: Number of motifs insertion per fork](./figures/plot_motif_insertion_per_fork.png){width=400}
\\n\\n</center>\\n\\n
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 15: Number of motifs insertion per strand](./figures/plot_motif_insertion_per_strand.png){width=400}
\\n\\n</center>\\n\\n
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 16: Number of motifs insertion per strand](./figures/plot_motif_insertion_per_fork_and_strand.png){width=400}
\\n\\n</center>\\n\\n
    ' >> report.rmd
    echo -e "\\n\\n<br /><br />\\n\\n#### Insertion site proportions\\n\\n" >> report.rmd
    echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 17: Proportion of motifs insertion per fork](./figures/plot_motif_insertion_per_fork_prop.png){width=500}
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 18: Proportion of motifs insertion per strand](./figures/plot_motif_insertion_per_strand_prop.png){width=500}
\\n\\n</center>\\n\\n
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure 19: Proportion of motifs insertion per strand](./figures/plot_motif_insertion_per_fork_and_strand_prop.png){width=500}
\\n\\n</center>\\n\\n
    ' >> report.rmd
    """
}


process plot_insertion { // sections 24.7.2 and 45 of the labbook 20200520, for TSS, section 47 20201211, for CDS section 48 20201211
    label 'r_ext_12cpu' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{*.png,*.pdf}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob 
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{plot_insertion_report.txt}", overwrite: false // 
    cache 'true'

    input:
    val file_name
    file pos from obs_rd_insertions_pos_ch1
    file freq from obs_rd_insertions_freq_ch1
    val tss
    val ess
    val cds
    val ori_coord
    val ter_coord
    val xlab
    val genome_size
    val prop_coding_genome
    val prop_ess_coding_genome
    val window_size
    val step
    file cute_file

    output:
    file "*.png" into fig_ch7 // warning: several files
    file "plot_insertion_report.txt"
    file "report.rmd" into log_ch20

    script:
    """
    echo -e "\\n\\n<br /><br />\\n\\n###  Insertion plots\\n\\n" > report.rmd
    plot_insertion.R "${pos}" "${freq}" "$tss" "$ess" "$cds" "${ori_coord}" "${ter_coord}" "${xlab}" "${genome_size}" "${prop_coding_genome}" "${prop_ess_coding_genome}" "${window_size}" "${step}" "${file_name}" "${task.cpus}" "${cute_file}" "plot_insertion_report.txt"
    echo -e "\\n\\n####  Histograms\\n\\n" >> report.rmd
    echo -e '
\\n\\n</center>\\n\\n
![Figure 20: Insertion site usage (total insertions).](./figures/plot_${file_name}_insertion_hist_tot.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure 21: Insertion site usage zoomed for sites with few insertions (total insertions).](./figures/plot_${file_name}_insertion_hist_tot_zoom.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure 22: Insertion site usage (forward strand).](./figures/plot_${file_name}_insertion_hist_forward.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure 23: Insertion site usage (reverse strand).](./figures/plot_${file_name}_insertion_hist_reverse.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
    ' >> report.rmd
    echo -e "\\n\\n<br /><br />\\n\\n####  Raw frequencies\\n\\n" >> report.rmd
    echo -e "\\n\\nSee the CL Labbook section 24.7.3 to explain the limitation around 100 bp\\n" >> report.rmd
    echo -e '
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure 24: Raw insertion frequencies.](./figures/plot_${file_name}_insertion_raw.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
    ' >> report.rmd
    echo -e "\\n\\n<br /><br />\\n\\n####  Binned frequencies\\n\\n" >> report.rmd
    count=1
    for i in $window_size ; do
        echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure \'\$((24 + \$count))\': frequencies using the binning range of \'\$(printf "%'d" \${i})\'](./figures/plot_${file_name}_insertion_bin_\'\${i}\'.png){width=600}
\\n\\n</center>\\n\\n
        ' >> report.rmd
        count=\$((count + 1))
        echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure \'\$((24 + \$count))\': frequencies using the binning range of \'\$(printf "%'d" \${i})\'](./figures/plot_${file_name}_lead_lag_insertion_bin_\'\${i}\'.png){width=600}
\\n\\n</center>\\n\\n
        ' >> report.rmd
        count=\$((count + 1))
    done
    if [[ ${tss} != "NULL" ]] ; then
        echo -e "\\n\\n<br /><br />\\n\\n###  Transcription start site (TSS) plots\\n\\n" >> report.rmd
        echo -e "\\n\\nSee the CL Labbook section 48.3 to to get the theoretical proportion of the codant/non codant essential/non essential genome\\n\\nSee the [plot_insertion_report.txt](./reports/plot_insertion_report.txt) file for details about the plotted values." >> report.rmd
        echo -e '
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((24 + \$count))\': Promoters per gene.](./figures/plot_${file_name}_promoter_per_genes.png){width=400}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((25 + \$count))\': Distance from TSS.](./figures/hist_${file_name}_tss_distance_freq.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((26 + \$count))\': Distance from TSS and Normal Law.](./figures/hist_${file_name}_tss_distance_freq_Nlaw.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
            \\n\\n<br /><br />\\n\\nThe number of insertions sites are indicated above graphs.\\n\\n
![Figure \'\$((27 + \$count))\': Insertion relative to TSS.](./figures/boxplot_${file_name}_tss.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((28 + \$count))\': Insertion relative to TSS without unknown.](./figures/boxplot_${file_name}_tss_wo_unknown.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
            \\n\\n<br /><br />\\n\\n###  Coding sequences (CDS) plots\\n\\nThe number of insertions sites inside CDS are indicated above graphs.\\n\\nSee the [plot_insertion_report.txt](./reports/plot_insertion_report.txt) file for details about the plotted values.
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((29 + \$count))\': Insertion relative to CDS.](./figures/boxplot_${file_name}_cds.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((30 + \$count))\': Insertion relative to CDS without unknown.](./figures/boxplot_${file_name}_cds_wo_unknown.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
            \\n\\nWarning: the number of observed and random insertions indicated above graphs can be greater than those indicated above Figure \'\$((28 + \$count))\', since a position that overlaps two genes is counted twice (see the [plot_insertion_report.txt](./reports/plot_insertion_report.txt) file for details).\\n\\n
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((31 + \$count))\': Insertion per class of CDS.](./figures/barplot_${file_name}_all.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((32 + \$count))\': Kind of insertion relative to CDS.](./figures/barplot_${file_name}_all_relative.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((33 + \$count))\': Kind of insertion relative to CDS.](./figures/barplot_${file_name}_inside_outside.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
![Figure \'\$((34 + \$count))\': Kind of insertion relative to CDS.](./figures/barplot_${file_name}_ess_uness.png){width=600}
\\n\\n</center>\\n\\n<br /><br />\\n\\n
        ' >> report.rmd
    fi
    """
    // single quotes required because of the !
}


process goalign {
    label 'goalign'
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{alignment.html}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob 
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{goalign_report.txt}", overwrite: false // 
    cache 'deep'

    input:
    file fasta from extract_seq_dup_ch2

    output:
    file "alignment.html" into fig_ch8
    file "goalign_report.txt"

    script:
    """
    # Remove duplicated data in a fasta file according to duplicated header
    awk '
        /^>/{f=!d[\$1];d[\$1]=1}f
    ' ${fasta} > tempo

    # split the fasta file according to forward or reverse sequences
    PATTERN='LEADING_16|LAGGING_16'
    awk -v var1=\$PATTERN '
        BEGIN{ORS="\\n"}
        {lineKind=(NR-1)%2}
        lineKind==0{record=\$0 ; next}
        lineKind==1{
            toGet=(record ~ var1)
            if(toGet){
                print record > "reverse.fasta"
                print \$0 > "reverse.fasta"
            }else{
                print record > "forward.fasta"
                print \$0 > "forward.fasta"
            }
            next
        }
    ' tempo

    # Goalign
    if [ -s reverse.fasta ] ; then
        goalign revcomp --unaligned -i reverse.fasta -o tempo2 # rev-comp the 16 sequences
        cat forward.fasta tempo2 > final.fasta
    else # we cannot have neither reverse nor forward
        cp forward.fasta final.fasta
    fi
    # add a hyphen before or after the sequence, to have correct alignment
    awk -v var1=\$PATTERN '
        BEGIN{ORS="\\n"}
        {lineKind=(NR-1)%2}
        lineKind==0{record=\$0 ; print \$0 ; next}
        lineKind==1{
            toGet=(record ~ var1)
            if(toGet){
                print "-"\$0 ; next
            }else{
                print \$0"-" ; next
            }
        }
    ' final.fasta > tempo3
    goalign draw biojs --auto-detect -i tempo3 -o alignment.html |& tee -a goalign_report.txt
    """
}


process dup_insertion_and_logo { // 52-53 of labbbok 20211115 and section 27 of labbook 20200907
    // warning: global logo png file already made above in the global_logo process
    // also fusion the 4 lead / lag / 0 / 16 insertions files
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/figures", mode: 'copy', pattern: "{*.png}", overwrite: false
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{dup_insertion_and_logo_report.txt}", overwrite: false // 
    cache 'true'

    input:
    val file_name
    val tss
    val window_size
    file freq from freq_dup_ch1
    file selected_freq from freq_dup_selected_ch1
    file pos from pos_dup_nonselected_ch // just for the line number computation
    file pos_selected from pos_dup_selected_ch // just for the line number computation
    val ori_coord
    val ter_coord
    val genome_size
    val xlab
    val insertion_dist
    val nb_max_insertion_sites
    file cute_file

    output:
    file "*.png" into fig_ch9 // warning: 2 files
    file "dup_insertion_and_logo_report.txt"
    file "report.rmd" into log_ch21

    script:
    """
    echo -e "\\n\\n<br /><br />\\n\\n###  Analysis with duplicates\\n\\n" > report.rmd
    dup_insertion_and_logo.R "${freq}" "${selected_freq}" "${ori_coord}" "${ter_coord}" "${genome_size}" "${xlab}" "${file_name}" "dup" "${insertion_dist}" "${cute_file}" "dup_insertion_and_logo_report.txt" # logo

    echo -e "\\n\\nSee the [${pos_selected}](./files/${pos_selected}) and [${selected_freq}](./files/${selected_freq}) files\\n\\n" >> report.rmd
    echo -e "\\n\\nWarning: more than the ${nb_max_insertion_sites} most frequent used sites can be present in the case of frequency equality\\n\\n" >> report.rmd

    pos_nb=\$(( \$(wc -l ${pos} | cut -f1 -d' ') - 1 )) # -1 because first line is the header
    echo -e "\\n\\nNumber of total positions using duplicated reads: \$(printf "%'d" \${pos_nb})\\n" >> report.rmd

    freq_nb=\$(( \$(wc -l ${freq} | cut -f1 -d' ') - 1 )) # -1 because first line is the header
    echo -e "\\n\\nNumber of different positions using duplicated reads: \$(printf "%'d" \${freq_nb})\\n" >> report.rmd

    pos_selected_nb=\$(wc -l ${pos_selected} | cut -f1 -d' ')
    echo -e "\\n\\nNumber of total positions after selection of the ${nb_max_insertion_sites} highest used sites: \$(printf "%'d" \${pos_selected_nb})\\n" >> report.rmd

    freq_selected_nb=\$(( \$(wc -l ${selected_freq} | cut -f1 -d' ') - 1 )) # -1 because first line is the header
    echo -e "\\n\\nNumber of different positions after selection of the ${nb_max_insertion_sites} highest used sites: \$(printf "%'d" \${freq_selected_nb})\\n" >> report.rmd

    TEMPO=(${window_size})
    FIG_NB_BEFORE=\$((\$(echo \${#TEMPO[@]}) * 2)) # nb of elements in the window size * nb of figure plotted
    if [[ ${tss} != "NULL" ]] ; then
        FIG_NB=\$(( 34 + \$FIG_NB_BEFORE + 1 + 1)) # 2 * because two figures
    else
        FIG_NB=\$(( 24 + \$FIG_NB_BEFORE + 1 + 1))
    fi
    echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure \'\$FIG_NB\': With duplicates raw insertion frequencies.](./figures/plot_${file_name}_insertion_dup_raw.png){width=600}
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure \'\$(echo \$((\$FIG_NB + 1)))\': Selected sites (${nb_max_insertion_sites} most used insertion sites).](./figures/plot_${file_name}_insertion_dup_selected.png){width=600}
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure \'\$(echo \$((\$FIG_NB + 2)))\': Insertion site usage (total insertions).](./figures/plot_${file_name}_insertion_hist_tot_selected.png){width=600}
\\n\\n<br /><br />\\n\\n</center>\\n\\n
    ' >> report.rmd
    echo -e "\\n\\nSelected sites with frequencies:\\n\\n" >> report.rmd
    echo "
\\`\\`\\`{r, echo = FALSE}
tempo <- read.table('./files/${selected_freq}', header = TRUE, colClasses = 'character', sep = '\\t', check.names = FALSE) ; 
kableExtra::kable_styling(knitr::kable(tempo, row.names = TRUE, digits = 0, caption = NULL, format='html'), c('striped', 'bordered', 'responsive', 'condensed'), font_size=10, full_width = FALSE, position = 'left')
\\`\\`\\`
    \n\n
    " >> report.rmd
    echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure \'\$(echo \$((\$FIG_NB + 3)))\': Alignment of the selected sites (click [here](./figures/alignment.html) to extend)](./figures/alignment.html){width=600}
\\n\\n</center>\\n\\n
    ' >> report.rmd
    echo -e "\\n\\n<br /><br />\\n\\nWarning: the frequency of each position is taken into account in the logo plot\\n\\n" >> report.rmd
    echo -e '
\\n\\n<br /><br />\\n\\n</center>\\n\\n
![Figure \'\$(echo \$((\$FIG_NB + 4)))\': With duplicates ${file_name} global logo on selected sites](./figures/global_logo_dup_${file_name}.png){width=600}
\\n\\n</center>\\n\\n
    ' >> report.rmd
    """
    // single quotes required because of the !
}



process backup {
    label 'bash' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{*.config,*.log}", overwrite: false // since I am in mode copy, all the output files will be copied into the publishDir. See \\wsl$\Ubuntu-20.04\home\gael\work\aa\a0e9a739acae026fb205bc3fc21f9b
    cache 'false'

    input:
    file config_file
    file log_file

    output:
    file "${config_file}" // warning message if we use file config_file
    file "${log_file}" // warning message if we use file log_file
    file "report.rmd" into log_ch22

    script:
    """
    echo -e "\\n\\n<br /><br />\\n\\n###  Backup\\n\\n" > report.rmd
    echo -e "See the [reports](./reports) folder for all the details of the analysis, including the parameters used in the .config file" >> report.rmd
    echo -e "\\n\\nFull .nextflow.log is in: ${launchDir}<br />The one in the [reports](./reports) folder is not complete (miss the end)" >> report.rmd
    """
}


process workflowVersion { // create a file with the workflow version in out_path
    label 'bash' // see the withLabel: bash in the nextflow config file 
    cache 'false'

    output:
    file "report.rmd" into log_ch23

    script:
    """
    modules=$modules # this is just to deal with variable interpretation during the creation of the .command.sh file by nextflow. See also \$modules below
    echo -e "\\n\\n<br /><br />\\n\\n###  Workflow Version\\n\\n" > report.rmd
    echo -e "\\n\\n#### General\\n\\n
| Variable | Value |
| :-- | :-- |
| Project<br />(empty means no .git folder where the main.nf file is present) | \$(git -C ${projectDir} remote -v | head -n 1) | # works only if the main script run is located in a directory that has a .git folder, i.e., that is connected to a distant repo
| Git info<br />(empty means no .git folder where the main.nf file is present) | \$(git -C ${projectDir} describe --abbrev=10 --dirty --always --tags) | # idem. Provide the small commit number of the script and nextflow.config used in the execution
| Cmd line | ${workflow.commandLine} |
| execution mode | ${system_exec} |" >> report.rmd 

    if [[ ! -z \$modules ]] ; then
        echo "| loaded modules (according to specification by the user thanks to the --modules argument of main.nf) | ${modules} |" >> report.rmd
    fi
    
    echo "| Manifest's pipeline version | ${workflow.manifest.version} |
| result path | ${out_path} |
| nextflow version | ${nextflow.version} |
    " >> report.rmd

    echo -e "\\n\\n<br /><br />\\n\\n#### Implicit variables\\n\\n
| Name | Description | Value | 
| :-- | :-- | :-- |
| launchDir | Directory where the workflow is run | ${launchDir} |
| nprojectDir | Directory where the main.nf script is located | ${projectDir} |
| workDir | Directory where tasks temporary files are created | ${workDir} |
    " >> report.rmd

    echo -e "\\n\\n<br /><br />\\n\\n#### User variables\\n\\n
| Name | Description | Value | 
| :-- | :-- | :-- |
| out_path | output folder path | ${out_path} |
| in_path | input folder path | ${in_path} |
    " >> report.rmd

    echo -e "\\n\\n<br /><br />\\n\\n#### Workflow diagram\\n\\nSee the [nf_dag.png](./reports/nf_dag.png) file" >> report.rmd
    """
}
//${projectDir} nextflow variable
//${workflow.commandLine} nextflow variable
//${workflow.manifest.version} nextflow variable
//Note that variables like ${out_path} are interpreted in the script block


process print_report { // section 8.8 of the labbook 20200520
    label 'r_ext' // see the withLabel: bash in the nextflow config file 
    publishDir "${out_path}", mode: 'copy', pattern: "{*.html}", overwrite: false // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    publishDir "${out_path}/reports", mode: 'copy', pattern: "{*.txt,*.rmd}", overwrite: true // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    cache 'false'

    input:
    file cute_file
    file report from log_ch0.concat(log_ch1,log_ch2, log_ch3, log_ch4, log_ch5, log_ch6, log_ch7, log_ch8, log_ch9, log_ch10, log_ch11, log_ch12, log_ch13, log_ch14, log_ch15, log_ch16, log_ch17, log_ch18, log_ch19, log_ch20, log_ch21, log_ch22, log_ch23).collectFile(name: 'report.rmd', sort: false)
    tuple val ("stat_tempo_name"), file ("stat_tempo") from stat_fastq_5p_filter_ch2
    file table1 from motif_table_ch // warning: several files
    file table2 from freq_dup_selected_ch2
    file png1 from fig_ch1
    file png2 from fig_ch2.collect() // warning: several files
    file png3 from fig_ch3.collect() // warning: several files
    file png4 from fig_ch4.collect() // warning: several files
    file png5 from fig_ch5.collect() // warning: several files
    file png6 from fig_ch6.collect() // warning: several files
    file png7 from fig_ch7.collect() // warning: several files
    file png8 from fig_ch8
    file png9 from fig_ch9.collect() // warning: several files
    file html from multiqc_ch

    output:
    file "report.html"
    file "print_report.txt"
    file "report.rmd"

    script:
    """
    cp ${report} report_file.rmd # this is to get hard files, not symlinks
    mkdir figures
    mkdir files
    mkdir reports
    cat ${stat_tempo} > ./files/${stat_tempo_name} # this is to get hard files, not symlinks
    cp ${table1} ${table2} ./files/ # this is to get hard files, not symlinks
    cp ${png1} ${png2} ${png3} ${png4} ${png5} ${png6} ${png7} ${png8} ${png9} ./figures/ # Warning several files
    cp ${png1} ./reports/nf_dag.png # trick to delude the knitting during the print report
    cp ${html} ./reports/ # this is to get hard files from html from multiqc_ch, not symlinks
    print_report.R "${cute_file}" "report_file.rmd" "print_report.txt"
    """
}


//////// end Processes
