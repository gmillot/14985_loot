#!/usr/bin/env bash

#########################################################################
##                                                                     ##
##     cutoff.sh                                                       ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################




input_file=$1
nb=$2
output_file=$3
log=$4



echo -e "<br /><br />\n\n### Selection of reads over ${nb} bases\n\n" > ${log}

# cutoff
awk -v var1=${nb} '{lineKind=(NR-1)%4}lineKind==0{record=$0; next}lineKind==1{toGet=(length($0)>=var1); if(toGet) print record}toGet' ${input_file} > ${output_file}_cutoff.fq
# end cutoff

# sequences length
cat ${output_file}_cutoff.fq | awk '{lineKind=(NR-1)%4}lineKind==1{print length($0)}' > ${output_file}_cutoff.length
# end sequences length

LC_NUMERIC="en_US.UTF-8" # this is to have printf working for comma thousand separator
line_nb_before=$(cat ${input_file} | wc -l)
line_nb_after=$(cat ${output_file}_cutoff.fq | wc -l)
echo -e "\n\nNumber of sequences before cutoff at ${nb} nucleotids: $(printf "%'d" $((${line_nb_before} / 4)))\n" >> ${log}
echo -e "Number of sequences after cutoff at ${nb} nucleotids: $(printf "%'d" $((${line_nb_after} / 4)))\n" >> ${log}
echo -e "Ratio: " >> ${log}
echo -e $(printf '%.2f\n' $(echo $" $line_nb_after / $line_nb_before " | bc -l)) >> ${log} # the number in '%.2f\n' is the number of decimals
echo -e "\n\n" >> ${log}




