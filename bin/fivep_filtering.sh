#!/usr/bin/env bash

#########################################################################
##                                                                     ##
##     fivep_filtering.sh                                              ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################



# Reminder : fastq files always present sequences in 5' to 3', whatever the strand.



input_file=$1
output_file=$2
fivep_seq_filtering=$3
fivep_seq_nb=$4
added_nb=$5
sum_of_2_nb=$6
attc_seq=$7
log=$8


echo -e "<br /><br />\n\n### Selection of reads with the attC in 5' and trimming of this sequence\n\n" > ${log}
# fastq filtering
awk -v var1=${fivep_seq_filtering} '
    {lineKind=(NR-1)%4;}
    lineKind==0{
        record=$0
        next
    }
    lineKind==1{
        toGet=$0 ~ var1
        if(toGet){print record}
    }
    toGet
' ${input_file} > tempo.fq # see protocol 44
# end fastq filtering


# printing the result
LC_NUMERIC="en_US.UTF-8" # this is to have printf working for comma thousand separator
line_nb_before=$(($(cat ${input_file} | wc -l) / 4)) # divided by 4 because it is a fastq
line_nb_after=$(($(cat tempo.fq | wc -l) / 4)) # divided by 4 because it is a fastq
echo -e "\n\n" >> ${log}
echo -e "Number of sequences before 5' filtering using ${fivep_seq_filtering}: $(printf "%'d" ${line_nb_before})\n" >> ${log}
echo -e "Number of sequences after 5' filtering: $(printf "%'d" ${line_nb_after})\n" >> ${log}
echo -e "Ratio: " >> ${log}
printf '%.2f\n' $(echo $" $line_nb_after / $line_nb_before " | bc -l) >> ${log} # the number in '%.2f\n' is the number of decimals
echo -e "\n\n" >> ${log}
# end printing the result


# recovering all the filtered sequences from the fastq
cat tempo.fq | awk '{lineKind=(NR-1)%4}lineKind==1{print $0}' | cut -c1-${sum_of_2_nb} > tempo.sq # filtered sequences 
# end recovering all the filtered sequences from the fastq

# making the head of the final stat file
header=$(echo $attc_seq | awk -v added_nb="$added_nb" '{
    FS="" ; OFS = "" ; ORS=""
}{
    print $0
    for (i=1;i<=added_nb;i++) {print "N"}
}')
# end making the head of the final stat file


# nucleotide frequencies of the 5' part of sequences
# from https://bioinformatics.stackexchange.com/questions/3089/calculating-nucleotide-frequency-per-column
gawk -v var1=$header 'BEGIN{
    ORS = ""
    split(var1, array1, "")
    for (key in array1) {print "\t"array1[key]}
    print "\n"
    ORS = "\n" ; OFS = "\t"
}{
    L=length($0);
    for(i=1;i<=L;i++) {
        B=substr($0,i,1);
        T[i][B]++;
    }
}END{
    for(BI=0;BI<5;BI++) {
        B=(BI==0?"A":(BI==1?"C":(BI==2?"G":(BI==3?"T":"N"))));
        printf("%s",B); 
        for(i in T) {
            tot=0.0;
            for(B2 in T[i]){
                tot+=T[i][B2];
            }
        printf("\t%0.2f",(T[i][B]/tot));
        } 
    printf("\n");
    }
}' tempo.sq > ${output_file}_5pAttc_1-${sum_of_2_nb}.stat
# nucleotide frequencies of the 5' part of sequences


# printing the result
echo -e "<br /><br />\n\n### Preparation of the graph of base frequencies\n\n" >> ${log}
echo -e "\n\nLength of the expected attC sequence at the 5' part of reads: ${fivep_seq_nb}\n\n" >> ${log}
echo -e "\n\nNumber of bases added after the expected attC sequence at the 5' part of reads, for graphical purpose: ${added_nb}\n\n" >> ${log}
echo -e "\n\nFrequencies of the graph are in the [${output_file}_5pAttc_1-${sum_of_2_nb}.stat](./files/${output_file}_5pAttc_1-${sum_of_2_nb}.stat) file\n\n" >> ${log}
# cat ${output_file}_5pAttc_1-${fivep_seq_nb}.stat >> ${log}
echo -e "\n\n" >> ${log}
# end printing the result


# sequences length
cat ${input_file} | awk '{lineKind=(NR-1)%4}lineKind==1{print length($0)}' > ${output_file}_ini.length
cat tempo.fq | awk '{lineKind=(NR-1)%4}lineKind==1{print length($0)}' > ${output_file}_5pAttc.length
# end sequences length


# trimming the 5' part of filtered sequences in the fastq
cat tempo.fq | awk -v var1=${fivep_seq_nb} '{
        lineKind=(NR-1)%4
    }
    lineKind==0{print $0}
    lineKind==1{print substr($0, var1+1)}
    lineKind==2{print $0}
    lineKind==3{print substr($0, var1+1)
}' > ${output_file}_5pAtccRm.fq
# end trimming the 5' part of filtered sequences in the fastq

# sequences length
cat ${output_file}_5pAtccRm.fq | awk '{lineKind=(NR-1)%4}lineKind==1{print length($0)}' > ${output_file}_5pAtccRm.stat
# end sequences length
