#!/usr/bin/env bash

#########################################################################
##                                                                     ##
##     trim.sh                                                         ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################



# $1 in_path
# $2 out_path
input_file=$1
output_file=$2
primer_fasta=$3
alientrimmer_l_param=$4
log=$5

echo -e "<br /><br />\n\n### Trim of the read for the primer parts\n\n" > ${log}
# sed '/^>.*$/d' ref_seq/adapters_TruSeq_B2699.fasta > tempo.adap.seq #in case we want to remove titles of the fasta files, but no need for AlienTrimmer
gzip ${input_file} -dc > input_file2
AlienTrimmer -i input_file2 -c ${primer_fasta} -o ${output_file} -l ${alientrimmer_l_param} | tee tempo.txt
# rm tempo.txt # not removed to be able to use -resume
sed -i -e ':a;N;$!ba;s/\n/\n<br \/>/g' tempo.txt # equivalent to sed -e ':a' -e 'N' -e '$!ba' -e 's/\n/ /g' file, see https://stackoverflow.com/questions/1251999/how-can-i-replace-each-newline-n-with-a-space-using-sed
cat tempo.txt >> ${log}

line_nb_before=$(zcat ${input_file} | wc -l)
line_nb_after=$(cat ${output_file} | wc -l) #not compressed anymore

LC_NUMERIC="en_US.UTF-8" # this is to have printf working for comma thousand separator
echo -e "<br /><br />AlienTrimmer also removes reads according to quality criteria\n" >> ${log}
echo -e "Number of sequence before trimming: $(printf "%'d" $((${line_nb_before} / 4)))\n" >> ${log}
echo -e "Number of sequences after trimming: $(printf "%'d" $((${line_nb_after} / 4)))\n" >> ${log}
echo -e "Ratio: " >> ${log}
printf '%.2f\n' $(echo $" $line_nb_after / $line_nb_before " | bc -l) >> ${log} # the number in '%.2f\n' is the number of decimals
echo -e "\n\n" >> ${log}




