#!/usr/bin/env Rscript

#########################################################################
##                                                                     ##
##     final_insertion_files.R                                          ##
##                                                                     ##
##     Gael A. Millot                                                  ##
##     Bioinformatics and Biostatistics Hub                            ##
##     Computational Biology Department                                ##
##     Institut Pasteur Paris                                          ##
##                                                                     ##
#########################################################################




################################ Aim


################################ End Aim


################################ Introduction


################################ End Introduction


################################ Acknowlegments


################################ End Acknowlegments


################################ Initialization


# R version checking
if(version$version.string != "R version 4.1.2 (2021-11-01)"){
    stop(paste0("\n\n================\n\nERROR IN plot_read_length.R\n", version$version.string, " IS NOT THE 4.1.2 RECOMMANDED\n\n================\n\n"))
}
# other initializations
erase.objects = TRUE # write TRUE to erase all the existing objects in R before starting the algorithm and FALSE otherwise. Beginners should use TRUE
if(erase.objects == TRUE){
    rm(list = ls(all.names = TRUE))
    erase.objects = TRUE
}
erase.graphs = TRUE # write TRUE to erase all the graphic windows in R before starting the algorithm and FALSE otherwise
script <- "final_insertion_files"


################################ End Initialization


################################ Parameters that need to be set by the user


################################ End Parameters that need to be set by the user


################################ Config import


tempo.cat <- "KIND OF RUN (SCRIPT, COPY-PASTE OR SOURCE): "
if(interactive() == FALSE){ # if(grepl(x = commandArgs(trailingOnly = FALSE), pattern = "R\\.exe$|\\/R$|Rcmd\\.exe$|Rcmd$|Rgui\\.exe$|Rgui$|Rscript\\.exe$|Rscript$|Rterm\\.exe$|Rterm$")){ # detection of script usage
    run.way <- "SCRIPT"
    cat(paste0("\n\n", tempo.cat, run.way, "\n"))
    command <- paste0(commandArgs(trailingOnly = FALSE), collapse = ",") # recover the full command
    args <- commandArgs(trailingOnly = TRUE) # recover arguments written after the call of the R script
    if(any(is.na(args))){
        stop(paste0("\n\n================\n\nERROR IN final_insertion_files.R\nTHE args OBJECT HAS NA\n\n================\n\n"), call. = FALSE)
    }
    tempo.arg.names <- c(
        "pos", 
        "ori_coord", 
        "ter_coord", 
        "nb_max_insertion_sites", 
        "file_name", 
        "cute", 
        "log"
    ) # objects names exactly in the same order as in the bash code and recovered in args. Here only one, because only the path of the config file to indicate after the final_insertion_files.R script execution
    if(length(args) != length(tempo.arg.names)){
        stop(paste0("\n\n================\n\nERROR IN final_insertion_files.R\nTHE NUMBER OF ELEMENTS IN args (", length(args),") IS DIFFERENT FROM THE NUMBER OF ELEMENTS IN tempo.arg.names (", length(tempo.arg.names),")\nargs:", paste0(args, collapse = ","), "\ntempo.arg.names:", paste0(tempo.arg.names, collapse = ","), "\n\n================\n\n"), call. = FALSE)
    }
    for(i1 in 1:length(tempo.arg.names)){
        assign(tempo.arg.names[i1], args[i1])
    }
    rm(tempo.arg.names, args, i1)
}else if(sys.nframe() == 0L){ # detection of copy-paste/direct execution (for debugging). With script it is also 0, with source, it is 4
    run.way <- "COPY-PASTE"
    cat(paste0("\n\n", tempo.cat, run.way, "\n"))
}else{
    run.way <- "SOURCE" # using source(), sys.nframe() is 4
    cat(paste0("\n\n", tempo.cat, run.way, "\n"))
}
rm(tempo.cat)


################################ End Config import

################################ Test

# cat("\n\n!!!!!!!!!!!!!!!!!!! WARNING: test values are activated\n\n")
# pos <- "C:/Users/Gael/Documents/Git_projects/14985_loot/dataset/test.fastq2_q20.pos" 
# ori_coord <- "2320711 2320942"
# ter_coord <- "4627368 4627400"
# nb_max_insertion_sites <- "6"
# file_name <- "caca"
# cute <- "https://gitlab.pasteur.fr/gmillot/cute_little_R_functions/-/raw/v11.2.0/cute_little_R_functions.R" 
# log <- "report.txt"



################################ end Test

################################ Recording of the initial parameters


param.list <- c(
    "erase.objects", 
    "erase.graphs", 
    "script", 
    "run.way",
    if(run.way == "SCRIPT"){"command"}, 
    "pos", 
    "ori_coord", 
    "ter_coord", 
    "nb_max_insertion_sites", 
    "file_name", 
    "cute", 
    "log"
)
if(any(duplicated(param.list))){
    stop(paste0("\n\n================\n\nINTERNAL CODE ERROR 1 IN final_insertion_files.R\nTHE param.list OBJECT CONTAINS DUPLICATED ELEMENTS:\n", paste(param.list[duplicated(param.list)], collapse = " "), "\n\n================\n\n"), call. = FALSE) # message for developers
}
if(erase.objects == TRUE){
    created.object.control <- ls()[ ! ls() %in% "param.list"]
    if( ! (all(created.object.control %in% param.list) & all(param.list %in% created.object.control))){
        stop(paste0("\n\n================\n\nINTERNAL CODE ERROR 2 IN final_insertion_files.R\nINCONSISTENCIES BETWEEN THE ARGUMENTS USED AND THE PARAMETERS REQUIRED IN THE EXECUTABLE CODE FILE\nTHE ARGUMENTS NOT PRESENT IN THE EXECUTABLE FILE (final_insertion_files.R) ARE:\n", paste(created.object.control[ ! created.object.control %in% param.list], collapse = " "), "\nTHE PARAMETERS OF THE EXECUTABLE FILE (final_insertion_files.R) NOT PRESENT IN THE ARGUMENTS ARE:\n", paste(param.list[ ! param.list %in% created.object.control], collapse = " "), "\n\n================\n\n"), call. = FALSE) # message for developers
    }
}
char.length <- nchar(param.list)
space.add <- max(char.length) - char.length + 5
param.ini.settings <- character(length = length(param.list))
for(i in 1:length(param.list)){
    param.ini.settings[i] <- paste0("\n", param.list[i], paste0(rep(" ", space.add[i]), collapse = ""), paste0(get(param.list[i]), collapse = ",")) # no env = sys.nframe(), inherit = FALSE in get() because look for function in the classical scope
}


################################ End Recording of the initial parameters


################################ Functions


# Functions are built such that they should have no direct use of Global objects (going through the R scope), and only use function arguments
# 1) Cute little function is sourced for the moment into the .GlobalEnv environment, but may be interesting to put it into a new environement just above .GlobalEnv environment. See https://stackoverflow.com/questions/9002544/how-to-add-functions-in-an-existing-environment
# 2) Argument names of each function must not be a name of Global objects (error message otherwise)
# 3) Argument name of each function ends with "_fun" in the first function, "_2fun" in the second, etc. This prevent conflicts with the argument partial names when using these functions, notably when they are imbricated


################ import functions from cute little functions toolbox


if(length(cute) != 1){
    stop(paste0("\n\n============\n\nERROR IN final_insertion_files.R\ncute PARAMETER MUST BE LENGTH 1: ", paste(cute, collapse = " "), "\n\n============\n\n"), call. = FALSE)
}else if(grepl(x = cute, pattern = "^http")){
    tempo.try <- try(suppressWarnings(suppressMessages(source(cute, local = .GlobalEnv))), silent = TRUE)
    if(any(grepl(x = tempo.try, pattern = "^[Ee]rror"))){
        stop(paste0("\n\n============\n\nERROR IN final_insertion_files.R\nHTTP INDICATED IN THE cute PARAMETER DOES NOT EXISTS: ", cute, "\n\n============\n\n"), call. = FALSE)
    }else{
        source(cute, local = .GlobalEnv) # source the fun_ functions used below
    }
}else if( ! grepl(x = cute, pattern = "^http")){
    if( ! file.exists(cute)){
        stop(paste0("\n\n============\n\nERROR IN final_insertion_files.R\nFILE INDICATED IN THE cute PARAMETER DOES NOT EXISTS: ", cute, "\n\n============\n\n"), call. = FALSE)
    }else{
        source(cute, local = .GlobalEnv) # source the fun_ functions used below
    }
}else{
    tempo.cat <- paste0("\n\n================\n\nINTERNAL CODE ERROR 3 IN final_insertion_files.R: CODE HAS TO BE MODIFIED\n\n============\n\n")
    stop(tempo.cat, call. = FALSE)
}


# required cute function checking
req.function <- c(
    "fun_check",
    "fun_pack", 
    "fun_df_remod", 
    "fun_report"
)
tempo <- NULL
for(i1 in req.function){
    if(length(find(i1, mode = "function")) == 0L){
        tempo <- c(tempo, i1)
    }
}
if( ! is.null(tempo)){
    tempo.cat <- paste0("ERROR IN final_insertion_files.R\nREQUIRED cute FUNCTION", ifelse(length(tempo) > 1, "S ARE", " IS"), " MISSING IN THE R ENVIRONMENT:\n", paste0(tempo, collapse = "()\n"))
    stop(paste0("\n\n================\n\n", tempo.cat, "\n\n================\n\n"), call. = FALSE) # == in stop() to be able to add several messages between ==
}
# end required function checking


################ local function: package import


# R Packages required
req.package.list <- c(
    "lubridate", 
    "ggplot2", 
    "lemon"
)
for(i in 1:length(req.package.list)){suppressMessages(library(req.package.list[i], character.only = TRUE))}
# fun_pack(req.package = req.package.list, load = TRUE, lib.path = NULL) # packages are imported even if inside functions are written as package.name::function() in the present code


################################ End Functions


################################ Pre-ignition checking


# reserved words
# end reserved words
# argument primary checking
arg.check <- NULL #
text.check <- NULL #
checked.arg.names <- NULL # for function debbuging: used by r_debugging_tools
ee <- expression(arg.check <- c(arg.check, tempo$problem) , text.check <- c(text.check, tempo$text) , checked.arg.names <- c(checked.arg.names, tempo$object.name))
tempo <- fun_check(data = pos, class = "vector", typeof = "character", length = 1) ; eval(ee)
tempo <- fun_check(data = ori_coord, class = "vector", typeof = "character", length = 1) ; eval(ee)
tempo <- fun_check(data = ter_coord, class = "vector", typeof = "character", length = 1) ; eval(ee)
tempo <- fun_check(data = nb_max_insertion_sites, class = "vector", typeof = "character", length = 1) ; eval(ee)
tempo <- fun_check(data = file_name, class = "vector", typeof = "character", length = 1) ; eval(ee)
tempo <- fun_check(data = cute, class = "vector", typeof = "character", length = 1) ; eval(ee)
tempo <- fun_check(data = log, class = "vector", typeof = "character", length = 1) ; eval(ee)
if(any(arg.check) == TRUE){ # normally no NA
    stop(paste0("\n\n================\n\n", paste(text.check[arg.check], collapse = "\n"), "\n\n================\n\n"), call. = FALSE) # == in stop() to be able to add several messages between == #
}
# end argument primary checking
# second round of checking and data preparation
# management of NA arguments
# end management of NA arguments
# management of NULL arguments
tempo.arg <-c(
    "pos", 
    "ori_coord", 
    "ter_coord", 
    "nb_max_insertion_sites", 
    "file_name", 
    "cute", 
    "log"
)
tempo.log <- sapply(lapply(tempo.arg, FUN = get, env = sys.nframe(), inherit = FALSE), FUN = is.null)
if(any(tempo.log) == TRUE){# normally no NA with is.null()
    tempo.cat <- paste0("ERROR IN final_insertion_files.R:\n", ifelse(sum(tempo.log, na.rm = TRUE) > 1, "THESE ARGUMENTS\n", "THIS ARGUMENT\n"), paste0(tempo.arg[tempo.log], collapse = "\n"),"\nCANNOT BE NULL")
    stop(paste0("\n\n================\n\n", tempo.cat, "\n\n================\n\n"), call. = FALSE) # == in stop() to be able to add several messages between ==
}
# end management of NULL arguments
# code that protects set.seed() in the global environment
# end code that protects set.seed() in the global environment
# warning initiation
ini.warning.length <- options()$warning.length
options(warning.length = 8170)
warn <- NULL
# warn.count <- 0 # not required
# end warning initiation
# other checkings

ori_coord <- strsplit(ori_coord, split = " ")[[1]]
if(length(ori_coord) != 2 & any(grepl(ori_coord, pattern = "\\D"))){# normally no NA with is.null()
    tempo.cat <- paste0("ERROR IN final_insertion_files.R:\nTHE ori_coord PARAMETER MUST BE TWO INTEGERS SEPARATED BY A SINGLE SPACE\nHERE IT IS: \n", paste0(ori_coord, collapse = " "))
    stop(paste0("\n\n================\n\n", tempo.cat, "\n\n================\n\n"), call. = FALSE) # == in stop() to be able to add several messages between ==
}else{
    ori_coord <- as.integer(ori_coord)
    if(any(is.na(ori_coord))){
        tempo.cat <- paste0("ERROR IN final_insertion_files.R:\nTHE CONVERSION OF THE ori_coord PARAMETER INTO INTEGER RETURNS NA: \n", paste0(ori_coord, collapse = " "))
        stop(paste0("\n\n================\n\n", tempo.cat, "\n\n================\n\n"), call. = FALSE)
    }
}
ter_coord <- strsplit(ter_coord, split = " ")[[1]]
if(length(ter_coord) != 2 & any(grepl(ter_coord, pattern = "\\D"))){# normally no NA with is.null()
    tempo.cat <- paste0("ERROR IN final_insertion_files.R:\nTHE ter_coord PARAMETER MUST BE TWO INTEGERS SEPARATED BY A SINGLE SPACE\nHERE IT IS: \n", paste0(ter_coord, collapse = " "))
    stop(paste0("\n\n================\n\n", tempo.cat, "\n\n================\n\n"), call. = FALSE) # == in stop() to be able to add several messages between ==
}else{
    ter_coord <- as.integer(ter_coord)
    if(any(is.na(ter_coord))){
        tempo.cat <- paste0("ERROR IN final_insertion_files.R:\nTHE CONVERSION OF THE ter_coord PARAMETER INTO INTEGER RETURNS NA: \n", paste0(ter_coord, collapse = " "))
        stop(paste0("\n\n================\n\n", tempo.cat, "\n\n================\n\n"), call. = FALSE)
    }
}
if(length(nb_max_insertion_sites) != 1 & any(grepl(nb_max_insertion_sites, pattern = "\\D"))){# normally no NA with is.null()
    tempo.cat <- paste0("ERROR IN final_insertion_files.R:\nTHE nb_max_insertion_sites PARAMETER MUST BE A SINGLE INTEGER\nHERE IT IS: \n", paste0(nb_max_insertion_sites, collapse = " "))
    stop(paste0("\n\n================\n\n", tempo.cat, "\n\n================\n\n"), call. = FALSE) # == in stop() to be able to add several messages between ==
}else{
    nb_max_insertion_sites <- as.integer(nb_max_insertion_sites)
    if(any(is.na(nb_max_insertion_sites))){
        tempo.cat <- paste0("ERROR IN final_insertion_files.R:\nTHE CONVERSION OF THE nb_max_insertion_sites PARAMETER INTO INTEGER RETURNS NA: \n", paste0(nb_max_insertion_sites, collapse = " "))
        stop(paste0("\n\n================\n\n", tempo.cat, "\n\n================\n\n"), call. = FALSE)
    }
}

# end other checkings
# reserved word checking
# end reserved word checking
# end second round of checking and data preparation
# package checking
# end package checking


################################ End pre-ignition checking


################################ Main code


################ Ignition


fun_report(data = paste0("\n\n################################################################ final_insertion_files PROCESS WITH FILE ", pos, "\n\n"), output = log, path = "./", overwrite = FALSE)
ini.date <- Sys.time()
ini.time <- as.numeric(ini.date) # time of process begin, converted into seconds
fun_report(data = paste0("\n\n################################ RUNNING DATE AND STARTING TIME\n\n"), output = log, path = "./", overwrite = FALSE)
fun_report(data = paste0(ini.date, "\n\n"), output = log, path = "./", overwrite = FALSE)
fun_report(data = paste0("\n\n################################ RUNNING\n\n"), output = log, path = "./", overwrite = FALSE)


################ End ignition


################ Graphical parameter initialization


################ End graphical parameter initialization


################ Data import


obs.ini <- read.table(pos, stringsAsFactors = FALSE) # no header


################ end Data import


############ modifications of imported tables

fun_report(data = paste0("\nHEAD OF THE INITAL FILE ", pos), output = log, path = "./", overwrite = FALSE)
fun_report(data = head(obs.ini), output = log, path = "./", overwrite = FALSE)

obs <- obs.ini # obs.ini will be only filtered for highest most frequent position and then returned for the seq_around_insertion process
names(obs) <- c("orient", "pos")
obs <- obs[2:1]

# ori and ter prep
ori_center <- mean(ori_coord, na.rm = TRUE)
ter_center <- mean(ter_coord, na.rm = TRUE)
# leading and lagging
obs <- data.frame(obs, fork = obs$orient, stringsAsFactors = TRUE)
if(ter_center > ori_center){
    obs$fork[obs$pos < ori_center | obs$pos > ter_center] <- abs(obs$fork[obs$pos < ori_center | obs$pos > ter_center] - 16) # left of the Ori and right of the dif (dif > ori) is switch 0 -> 16 and 16 -> 0 to have the leading and lagging
}else{
    obs$fork[obs$pos <= ori_center & obs$pos >= ter_center] <- abs(obs$fork[obs$pos <= ori_center & obs$pos >= ter_center] - 16) # right of the Ori and left of the dif (dif < ori) is switch 0 -> 16 and 16 -> 0 to have the leading and lagging
}
obs$fork<- factor(obs$fork, levels = c(0, 16), labels = c("Leading", "Lagging"))
obs <- data.frame(Sequence = "obs", Position = obs$pos, names = paste(obs$fork, obs$orient, sep = "_"), fork = obs$fork, orient = obs$orient)
obs$orient[obs$orient == 0] <- "Forward"
obs$orient[obs$orient == 16] <- "Reverse"
fun_report(data = paste0("\nHEAD OF THE MODIFIED FILE ", pos), output = log, path = "./", overwrite = FALSE)
fun_report(data = head(obs), output = log, path = "./", overwrite = FALSE)
fun_report(data = paste0("\nNUMBER OF OBS POSITIONS:\n", format(nrow(obs), big.mark=",")), output = log, path = "./", overwrite = FALSE)


# saving position file



res <- aggregate(x = obs$Position, by = list(Sequence = obs$Sequence, Position = obs$Position, names = obs$names, fork = obs$fork, orient = obs$orient), FUN = length)
names(res)[names(res) == "x"] <- "freq"
res.ini <- res
if( ! grepl(x = file_name, pattern = "^.*nodup.*$")){ # selection of the highest insertion usage sites
    if(nb_max_insertion_sites > nrow(res)){
            res <- res[order(res$freq, decreasing = TRUE)[1:nrow(res)], ]
            tempo.cat <- paste0("\nWARNING: nb_max_insertion_sites PARAMETER IS GREATER THAN THE NUMBER OF DIFFERENT SITES: \n", format(nrow(res), big.mark=","))
            fun_report(data = tempo.cat, output = log, path = "./", overwrite = FALSE)
            cat(tempo.cat)
    }else{
        tempo <- res[order(res$freq, decreasing = TRUE), ]
        tempo.limit <- tempo$freq[nb_max_insertion_sites]
        res <- tempo[tempo$freq >= tempo.limit, ] # did not take res[order(res$freq, decreasing = TRUE), ][1:nb_max_insertion_sites, ] in the case of freq equality
    }
    options(scipen = 1000) 
    write.table(res, file = paste0("./", file_name, "_annot_selected.freq"), row.names = FALSE, col.names = TRUE, append = FALSE, quote = FALSE, sep = "\t") # if duplicated -> selected data for highest usage # file for plotting selection insertion sites in duplicated reads
    options(scipen = 0)
    obs.ini <- obs.ini[obs.ini[ , 2] %in% res$Position, ] # obs.ini[ , 2] because no column names
}else{
    write.table(NULL, file = paste0("./", file_name, "_annot_selected.freq"), row.names = FALSE, col.names = TRUE, append = FALSE, quote = FALSE, sep = "\t")
}

options(scipen = 1000) # to avoid writing of scientific numbers in tables, see https://stackoverflow.com/questions/3978266/number-format-writing-1e-5-instead-of-0-00001
write.table(res.ini, file = paste0("./", file_name, "_annot.freq"), row.names = FALSE, col.names = TRUE, append = FALSE, quote = FALSE, sep = "\t") # if duplicated -> non selected data for highest usage # file for plotting all the insertion sites in duplicated and non duplicated reads
write.table(obs, file = paste0("./", file_name, "_annot.pos"), row.names = FALSE, col.names = TRUE, append = FALSE, quote = FALSE, sep = "\t") # if duplicated -> non selected data for highest usage # annotated file, just for line number computation (i.e., nb of insertion events) and also of random events
write.table(obs.ini, file = paste0("./", file_name, "_selected_if_dup.pos"), row.names = FALSE, col.names = FALSE, append = FALSE, quote = FALSE, sep = "\t") # if duplicated -> selected data for highest usage # non annotated file, for sequence extractions
options(scipen = 0)



############ end modifications of imported tables



################ Pdf window closing


################ end Pdf window closing


################ Seeding inactivation


set.seed(NULL)


################ end Seeding inactivation


################ Environment saving


save(list = ls(), file = "all_objects.RData")
fun_report(data = paste0("\n\n################################ RUNNING END"), output = log, path = "./", overwrite = FALSE)
end.date <- Sys.time()
end.time <- as.numeric(end.date)
total.lapse <- round(lubridate::seconds_to_period(end.time - ini.time))
fun_report(data = paste0("\n\nEND TIME: ", end.date), output = log, path = "./", overwrite = FALSE)
fun_report(data = paste0("\n\nTOTAL TIME LAPSE: ", total.lapse), output = log, path = "./", overwrite = FALSE)
fun_report(data = paste0("\n\nALL DATA SAVED IN all_objects.RData"), output = log, path = "./", overwrite = FALSE)


################ end Environment saving


################ Warning messages


fun_report(data = paste0("\n\n################################ RECAPITULATION OF WARNING MESSAGES"), output = log, path = "./", overwrite = FALSE)
if( ! is.null(warn)){
    fun_report(data = paste0("\n\n", warn), output = log, path = "./", overwrite = FALSE)
}else{
    fun_report(data = paste0("\n\nNO WARNING MESSAGE TO REPORT"), output = log, path = "./", overwrite = FALSE)
}


################ end Warning messages


################ Parameter printing


fun_report(data = paste0("\n\n################################ INITIAL SETTINGS OF PARAMETERS"), output = log, path = "./", overwrite = FALSE)
fun_report(data = param.ini.settings, output = log, path = "./", overwrite = FALSE, , vector.cat = TRUE)
fun_report(data = paste0("\n\n################################ R SYSTEM AND PACKAGES"), output = log, path = "./", overwrite = FALSE)
tempo <- sessionInfo()
tempo$otherPkgs <- tempo$otherPkgs[order(names(tempo$otherPkgs))] # sort the packages
tempo$loadedOnly <- tempo$loadedOnly[order(names(tempo$loadedOnly))] # sort the packages
fun_report(data = tempo, output = log, path = "./", overwrite = FALSE, , vector.cat = TRUE)
fun_report(data = paste0("\n\n################################ JOB END\n\nTIME: ", end.date, "\n\nTOTAL TIME LAPSE: ", total.lapse, "\n"), output = log, path = "./", overwrite = FALSE)


################ end Parameter printing


################################ End Main code







