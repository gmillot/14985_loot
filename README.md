[//]: # "#to make links in gitlab: example with racon https://github.com/isovic/racon"
[//]: # "tricks in markdown: https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown"


[![Nextflow](https://img.shields.io/badge/code-Nextflow-blue?style=plastic)](https://www.nextflow.io/)
&nbsp;
[![License: GPL-3.0](https://img.shields.io/badge/licence-GPL%20(%3E%3D3)-green?style=plastic)](https://www.gnu.org/licenses)



## TABLE OF CONTENTS


   - [AIM](#aim)
   - [WARNING](#warning)
   - [CONTENT](#content)
   - [INPUT](#input)
   - [HOW TO RUN](#how-to-run)
   - [OUTPUT](#output)
   - [WHOLE DATA REPOSITORY](#whole-data-repository)
   - [VERSIONS](#versions)
   - [LICENCE](#licence)
   - [CITATION](#citation)
   - [CREDITS](#credits)
   - [ACKNOWLEDGEMENTS](#Acknowledgements)
   - [WHAT'S NEW IN](#what's-new-in)

<br /><br />
## AIM

Detect the integrations of an integron cassette into the genome of procaryotes.

<br /><br />
## WARNINGS

Can be only used for specific experimental designs performed by Loot et al.

<br /><br />
## CONTENT

| 14985_loot folder | Description |
| --- | --- |
| **main.nf** | File that can be executed using a linux terminal, a MacOS terminal or Windows 10 WSL2. |
| **nextflow.config** | Parameter settings for the *main.nf* file. Users have to open this file, set the desired settings and save these modifications before execution. |
| **dataset** | Folder containing some datasets than can be used as examples in the **nextflow.config** file. |
| **example_of_results** | Folder containing examples of result obtained with the dataset. See the OUTPUT section for the description of the folder and files. |
| **Licence.txt** | Licence of the release. |

<br /><br />
## INPUT

- A zipped fastq file.
- A primer fasta file.
- A reference genome fasta file.
- A txt file of list of Transcription Starting Sites. 
- A tsv file of list of Essential genes.
<br />
See the **nextflow.config** file.


<br /><br />
## HOW TO RUN

### 1. Prerequisite

Installation of:<br />
[nextflow DSL2](https://github.com/nextflow-io/nextflow)<br />
[Graphviz](https://www.graphviz.org/download/), `sudo apt install graphviz` for Linux ubuntu<br />
[Apptainer](https://github.com/apptainer/apptainer)<br />

<br /><br />
### 2. Local running (personal computer)


#### 2.1. *main.nf* file in the personal computer

- Mount a server if required:

<pre>
DRIVE="Z"
sudo mkdir /mnt/share
sudo mount -t drvfs $DRIVE: /mnt/share
</pre>

Warning: if no mounting, it is possible that nextflow does nothing, or displays a message like:
<pre>
Launching `main.nf` [loving_morse] - revision: d5aabe528b
/mnt/share/Users
</pre>

- Run the following command from where the *main.nf* and *nextflow.config* files are (example: \\wsl$\Ubuntu-20.04\home\gael):

<pre>
nextflow run main.nf -c nextflow.config
</pre>

with -c to specify the name of the config file used.

<br /><br />
#### 2.3. *main.nf* file in the public gitlab repository

Run the following command from where you want the results:

<pre>
nextflow run -hub pasteur gmillot/14985_loot -r v1.0.0
</pre>

<br /><br />
### 3. Distant running (example with the Pasteur cluster)

#### 3.1. Pre-execution

Copy-paste this after having modified the EXEC_PATH variable:

<pre>
EXEC_PATH="/pasteur/zeus/projets/p01/BioIT/gmillot/14985_loot" # where the bin folder of the main.nf script is located
export CONF_BEFORE=/opt/gensoft/exe # on maestro

export JAVA_CONF=java/13.0.2
export JAVA_CONF_AFTER=bin/java # on maestro
export APP_CONF=apptainer/1.2.3
export APP_CONF_AFTER=bin/apptainer # on maestro
export GIT_CONF=git/2.39.1
export GIT_CONF_AFTER=bin/git # on maestro
export GRAPHVIZ_CONF=graphviz/2.42.3
export GRAPHVIZ_CONF_AFTER=bin/graphviz # on maestro

MODULES="${CONF_BEFORE}/${JAVA_CONF}/${JAVA_CONF_AFTER},${CONF_BEFORE}/${APP_CONF}/${APP_CONF_AFTER},${CONF_BEFORE}/${GIT_CONF}/${GIT_CONF_AFTER}/${GRAPHVIZ_CONF}/${GRAPHVIZ_CONF_AFTER}"
cd ${EXEC_PATH}
chmod 755 ${EXEC_PATH}/bin/*.* # not required if no bin folder
module load ${JAVA_CONF} ${APP_CONF} ${GIT_CONF} ${GRAPHVIZ_CONF}
</pre>

<br /><br />
#### 3.2. *main.nf* file in a cluster folder

Modify the second line of the code below, and run from where the *main.nf* and *nextflow.config* files are (which has been set thanks to the EXEC_PATH variable above):

<pre>
HOME_INI=$HOME
HOME="${ZEUSHOME}/14985_loot/" # $HOME changed to allow the creation of .nextflow into /$ZEUSHOME/14985_loot/, for instance. See NFX_HOME in the nextflow software script
trap '' SIGINT
nextflow run --modules ${MODULES} main.nf -c nextflow.config
HOME=$HOME_INI
trap SIGINT
</pre>

<br /><br />
#### 3.3. *main.nf* file in the public gitlab repository

Modify the first and third lines of the code below, and run (results will be where the EXEC_PATH variable has been set above):

<pre>
VERSION="v1.0"
HOME_INI=$HOME
HOME="${ZEUSHOME}/14985_loot/" # $HOME changed to allow the creation of .nextflow into /$ZEUSHOME/14985_loot/, for instance. See NFX_HOME in the nextflow software script
trap '' SIGINT
nextflow run --modules ${MODULES} -hub pasteur gmillot/14985_loot -r $VERSION -c $HOME/nextflow.config
HOME=$HOME_INI
trap SIGINT
</pre>

<br /><br />
### 4. Error messages and solutions

#### Message 1
```
Unknown error accessing project `gmillot/14985_loot` -- Repository may be corrupted: /pasteur/sonic/homes/gmillot/.nextflow/assets/gmillot/14985_loot
```

Purge using:
<pre>
rm -rf /pasteur/sonic/homes/gmillot/.nextflow/assets/gmillot*
</pre>

#### Message 2
```
WARN: Cannot read project manifest -- Cause: Remote resource not found: https://gitlab.pasteur.fr/api/v4/projects/gmillot%2F14985_loot
```

Contact Gael Millot (distant repository is not public).

#### Message 3

```
permission denied
```

Use chmod to change the user rights. Example linked to files in the bin folder: 
```
chmod 755 bin/*.*
```


<br /><br />
## OUTPUT

| 14985_loot_xxxxxx folder | Description |
| --- | --- |
| **report.html** | Report of the analysis. |
| **reports** | Folder containing all the reports of the different processes as well as the **nextflow.config** file used. |
| **files** | Folder containing some of the output files of the processes. |
| **figures** | Folder containing all the figures in the **report.html** in the .png format. |
| **fastQC1** | Folder containing the results of the first read QC, after removal of reads containing only N and after primer trimming by AlienTrimmer. |
| **fastQC2** | Folder containing the results of the second read QC, selection of reads with a specific sequence in 5' and after removing this sequence. |


<br /><br />
## WHOLE DATA REPOSITORY

[https://zenodo.org/records/10047271](https://zenodo.org/records/10047271)

<br /><br />
## VERSIONS


The different releases are tagged [here](https://gitlab.pasteur.fr/gmillot/14985_loot/-/tags)

<br /><br />
## LICENCE


This package of scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchandability or fitness for a particular purpose.
See the GNU General Public License for more details at https://www.gnu.org/licenses or in the Licence.txt attached file.

<br /><br />
## CITATION


Not yet published

<br /><br />
## CREDITS


[Gael A. Millot](https://gitlab.pasteur.fr/gmillot), Hub-CBD, Institut Pasteur, Paris, France

<br /><br />
## ACKNOWLEDGEMENTS


Frédéric Lemoine, Hub-CBD, Institut Pasteur, Paris

Bertrand Néron, Hub-CBD, Institut Pasteur, Paris

The developers & maintainers of the mentioned softwares and packages, including:

- [R](https://www.r-project.org/)
- [ggplot2](https://ggplot2.tidyverse.org/)
- [ggtree](https://yulab-smu.top/treedata-book/)
- [Nextflow](https://www.nextflow.io/)
- [Apptainer](https://apptainer.org/)
- [Docker](https://www.docker.com/)
- [Gitlab](https://about.gitlab.com/)
- [Bash](https://www.gnu.org/software/bash/)
- [Ubuntu](https://ubuntu.com/)


<br /><br />
## WHAT'S NEW IN

### v8.8.0

README.md file updated and dataset folder cleaned.


### v8.7.0

Precision about were the cutting is, relatively to the nucleotide position indicated in the files & logos improved for the reverse panels


### v8.6.0

Figure 21 and 44 modified


### v8.5.0

Some figures corrected, computations of number of insertions also and the duplicated part is now ok


### v8.4.0

Same bug fixed elsewhere


### v8.3.0

Small bug removed: now same orientation plotted in the CDS plots


### v8.2.0

All the problems should be solved in this version


### v8.1.0

Display improvement. But still problem when using the option -resume of nextflow run


### v8.0.0

- Many things added.
- Operational version for Ecoli. But Warning: still problem when using the option -resume of nextflow run, notably including a wrong figure 38, because of all the code between processes in the main.nf file that are not controled by the cache system


### v7.10.0

Pointing to the singularity folder improved, new option "slurm_local" added better worflow report


### v7.9.0

Pipeline improved


### v7.8.0

Pipeline improved


### v7.7.0

Kraken added and multiQC fixed


### v7.6.0

Everything's fine, except the multiQC


### v7.5.0

Ok up to the plot_insertion process, tested using the test file


### v7.4.0

Ok up to global logo, tested using the test file


### v7.3.0

flow and files improved by Fred, because of the relative paths, no need of dev/


### v7.2.0

better priority for slurm


### v7.1.0

dev/test.config adapted to slurm


### v7.0.0

Ok up to insertion freq, tested using the test file


### v6.1.0

config file debugged


### v6.0.0

Ok up to q20 tested using the test file


### v5.1.0

html report improved a bit


### v5.0.0

Now the results are compiled in a html report


### v4.1.0

Ok up to plot after attC read selection and attC trimming, tested on full B2699, and debugged


### v4.0.0

Ok up to plot after attC read selection and attC trimming, tested on full B2699


### v3.1.0

report improved


### v3.0.0

Ok up to plot after attC read selection and attC trimming


### v2.0.0

Ok up to fastq files QC


### v1.0.0

Backbone for nextflow scripts and config files



